<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRealDatetimeEndToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dateTime('completed_datetime_end')->after('datetime_end')->nullable();
            $table->dateTime('released_datetime_end')->after('completed_datetime_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            if (Schema::hasColumn('orders', 'completed_datetime_end')) {
                $table->dropColumn('completed_datetime_end');
            }
            if (Schema::hasColumn('orders', 'released_datetime_end')) {
                $table->dropColumn('released_datetime_end');
            }
        });
    }
}
