<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_cards', function (Blueprint $table) {
            $table->id();
            $table->string('number');
            $table->string('phone_number');
            $table->foreignId('agent_id')->nullable();
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('set null');
            $table->foreignId('agent_order_id')->nullable();
            $table->foreign('agent_order_id')->references('id')->on('agent_orders')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agent_cards', function (Blueprint $table) {
            $table->dropForeign(['agent_id']);
            $table->dropForeign(['agent_order_id']);
        });
        Schema::dropIfExists('agent_cards');
    }
}
