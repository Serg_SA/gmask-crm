<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string('brand');
            $table->string('model')->index();
            $table->string('year_from', 4)->nullable();
            $table->string('year_to', 4)->nullable();
            $table->timestamps();
        });

        $path = base_path('public\storage\csv\cars.csv');
        $file = str_replace('\\', '/', $path);

        $query = <<<EOF
LOAD DATA LOCAL INFILE '$file' 
INTO TABLE cars FIELDS TERMINATED BY ';' OPTIONALLY 
ENCLOSED BY '"' LINES TERMINATED BY '\\n' 
(brand, model, year_from, year_to, @created_at, @updated_at) 
SET 
created_at = NOW(),
updated_at = null, 
year_to= 
    CASE 
        WHEN year_to = '' 
        THEN null
        ELSE year_to 
    END
EOF;
        $pdo = DB::connection()->getPdo();
        $pdo->exec($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
