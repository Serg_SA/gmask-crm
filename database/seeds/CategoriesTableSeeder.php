<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Антигравийные комплекты',
            'Защита фар',
            'Защита лобового стекла',
            'Тонировка стёкол',
            'Химчистка салона',
            'Полировка кузова',
            'Керамика',
            'Чехлы',
            'Аквапринт',
            'Гравировка',
            'Защита салона',
            'Реставрация салона',
            'Шумоизоляция'
        ];

        foreach ($categories as $category) {
            Category::create(['name' => $category]);
        }
    }
}
