<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv_files = [
            'app/public/csv/clients-12-09-2020-49678_1.csv',
            'app/public/csv/clients-12-09-2020-49678_2.csv',
            'app/public/csv/clients-12-09-2020-49678_3.csv',
        ];
        foreach ($csv_files as $csv_file) {
            $file = storage_path($csv_file);
            $row = 0;
            if (($handle = fopen($file, 'r')) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ';')) !== FALSE) {
                    if ($row == 0) {
                        $row++;
                        continue;
                    }
                    $name = $data[0];
                    $phone = $data[1];
                    $data_user = [
                        'name' => $name,
//                        'email' => $phone . '@gmask.kz',
                        'phone' => '+'.$phone,
                        'password' => Hash::make($phone . '_' . $phone),
                    ];
                    $user = User::create($data_user);
                    DB::table('users')
                        ->where('id', $user->id)
                        ->update(['settings' => json_encode(['locale' => 'ru'])]);
                }
                fclose($handle);
            }
        }
    }
}
