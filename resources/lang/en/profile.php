<?php

return [
    'brand' => 'Brand',
    'model' => 'Model',
    'year_from' => 'Year From',
    'your_cars' => 'Your Cars',
    'your_car' => 'Your Car',
    'license_plate' => 'license Plate',
    'photos' => 'Photos',
    'dropzone_placeholder' => 'Drop files here to upload',
    'car' => 'Car',
    'back_to_profile' => 'Back to profile',
    'client_created_successfully' => 'Client created successfully',
];
