<?php

return [
    'brand' => 'Марка',
    'model' => 'Модель',
    'year_from' => 'Год выпуска этоого модельного ряда',
    'your_cars' => 'Ваши автомобили',
    'your_car' => 'Ваш автомобиль',
    'license_plate' => 'Номерной знак',
    'photos' => 'Фотографии',
    'dropzone_placeholder' => 'Перетащите файлы для загрузки',
    'car' => 'Автомобиль',
    'back_to_profile' => 'Вернуться в профиль',
    'client_created_successfully' => 'Клиент успешно создан',
];
