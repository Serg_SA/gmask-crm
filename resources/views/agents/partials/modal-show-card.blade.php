<div class="modal modal-success fade" tabindex="-1" id="showAgentCardModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="voyager-certificate"></i>
                    {{ __('generic.warranty_card') }} № <span class="header-number"></span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-edit-add" role="form" >
                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="col">

                        </div>


                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

@push('javascript')
    <script>
        $(document).ready(function () {

            // Modal
            (function () {
                const modal = document.getElementById('showAgentCardModal');
                if ( !modal )
                    return;

                // Event Listeners for buttons
                var buttonsModal = document.querySelectorAll('.btn-show-warranty');
                if ( buttonsModal.length > 0 ) {
                    buttonsModal.forEach( function (el) {
                        el.addEventListener('click', function(e) {
                            e.preventDefault();
                            orderNumberInput.value = e.target.dataset.orderNumber;
                            warrantyTemplateInput.value = e.target.dataset.warrantyTemplate;
                            metersInput.max = e.target.dataset.orderMetersLeft;
                            titleOrderNumber.firstElementChild.innerHTML = e.target.dataset.orderNumber;
                            titleWarrantyTemplate.firstElementChild.innerHTML = e.target.dataset.warrantyTemplate;
                            if ( orderNumberInput.value
                                && warrantyTemplateInput.value
                                && titleOrderNumber.firstElementChild.innerHTML
                                && titleWarrantyTemplate.firstElementChild.innerHTML
                            ) {
                                $(modal).modal('show');
                            }
                        });
                    });
                }

                $(modal).on('shown.bs.modal', function () {

                });

                $(modal).on('hidden.bs.modal', function () {
                    orderNumberInput.value = '';
                    warrantyTemplateInput.value = '';
                    titleOrderNumber.firstElementChild.innerHTML = '';
                    titleWarrantyTemplate.firstElementChild.innerHTML = '';
                });
            })();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Define variables

            // On submit form
            function getCardData(number) {
                $.ajax({
                    type:'POST',
                    url: '{{route('agent.cards.get_card_ajax')}}',
                    data: data,
                    beforeSend: function( xhr ) {
                        submitButton.classList.add('loading');
                    },
                    success: function(data) {
                        submitButton.classList.remove('loading');
                        $(submitButton).hide();
                        console.log(data);
                        if (data.success ?? data.card) {
                            console.log(data);
                            console.log(data.success);

                            alert.classList.add('alert-success');
                            alert.innerHTML = (data.success && data.card) ? 'Карта <strong>' + data.card + '</strong> успешно создана.' : '';
                            $(alert).show();
                            alertSystem.innerHTML = '<string>Подождите, идет перезагрузка страницы ...</string>';
                            $(alertSystem).show();
                            setTimeout(() => {
                                document.location.reload();
                            }, 2000);

                        }
                        if (data.error) {
                            console.log(data.error);
                            alert.classList.add('alert-danger');
                            alert.innerHTML = data.error;
                            $(alert).show();
                            setTimeout(function () {
                                let alertDanger = _this.querySelector('.alert-danger');
                                alertDanger.classList.remove('alert-danger');
                                $(alertDanger).hide();
                                alertDanger.innerHTML = '';
                                $('#addNewAgentCardModal').modal('hide');
                            }, 2000);
                        }
                    }
                });
            }
            form.addEventListener('submit', (e) => {
                e.preventDefault();
                let _this = e.target;

                let alert = _this.querySelector('.alert');
                let alertSystem = _this.querySelector('.alert-system');
                data = {
                    full_name: formData['full_name'].value,
                    master: formData['master'].value,
                    phone_number: formData['phone_number'].value,
                    car_brand_model: formData['car_brand_model'].value,
                    car_number: formData['car_number'].value,
                    warranty_template: formData['warranty_template'].value,
                    order_number: formData['order_number'].value,
                    meters: formData['meters'].value,
                };

                console.log(data)
                $.ajax({
                    type:'POST',
                    url: '{{route('agent.cards.store_ajax')}}',
                    data: data,
                    beforeSend: function( xhr ) {
                        submitButton.classList.add('loading');
                    },
                    success: function(data) {
                        submitButton.classList.remove('loading');
                        $(submitButton).hide();
                        console.log(data);
                        if (data.success ?? data.card) {
                            console.log(data);
                            console.log(data.success);

                            alert.classList.add('alert-success');
                            alert.innerHTML = (data.success && data.card) ? 'Карта <strong>' + data.card + '</strong> успешно создана.' : '';
                            $(alert).show();
                            alertSystem.innerHTML = '<string>Подождите, идет перезагрузка страницы ...</string>';
                            $(alertSystem).show();
                            setTimeout(() => {
                                document.location.reload();
                            }, 2000);

                        }
                        if (data.error) {
                            console.log(data.error);
                            alert.classList.add('alert-danger');
                            alert.innerHTML = data.error;
                            $(alert).show();
                            setTimeout(function () {
                                let alertDanger = _this.querySelector('.alert-danger');
                                alertDanger.classList.remove('alert-danger');
                                $(alertDanger).hide();
                                alertDanger.innerHTML = '';
                                $('#addNewAgentCardModal').modal('hide');
                            }, 2000);
                        }
                    }
                });
            });

        });
    </script>
@endpush
