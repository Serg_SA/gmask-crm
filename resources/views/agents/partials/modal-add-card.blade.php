<div class="modal modal-success fade" tabindex="-1" id="addNewAgentCardModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="voyager-certificate"></i>
                    {{ __('generic.add_new_warranty_card') }}
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body modal-info text-info">
                <div>
                    <span id="titleWarrantyTemplate">{{ __('generic.warranty_template') }}: <strong></strong></span>
                </div>
                <div>
                    <span id="titleOrderNumber">{{ __('generic.order_number') }}: <strong></strong></span>
                </div>

            </div>
            <div class="modal-body">
                <div class="form-edit-add" role="form" >
                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="col">
                            <form action="" id="formAddNewAgentCardAjax">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="full_name">{{ __('generic.full_name_cardholder') }}</label>
                                    <input type="text" class="form-control" id="full_name" name="full_name" placeholder=""
                                           value="{{ old('full_name') }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="phone_number">{{ __('generic.phone_number') }}</label>
                                    <input type="tel" class="form-control" id="phone_number" name="phone_number" placeholder="+77XXXXXXX"
                                           value="{{ old('phone_number') }}" required>
                                    <span class="validation-input-msg"></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="master">{{ __('generic.master') }}</label>
                                            <input type="text" class="form-control" id="master" name="master" placeholder=""
                                                   value="{{ old('master') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="meters">{{ __('generic.linear_meters_quantity') }}</label>
                                            <input type="number" class="form-control" id="meters" name="meters" placeholder=""
                                                   value="{{ old('meters') }}" min="1" max="50" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="car_brand_model">{{ __('generic.car_brand_model') }}</label>
                                            <input type="text" class="form-control" id="car_brand_model" name="car_brand_model" placeholder=""
                                                   value="{{ old('car_brand_model') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="car_number">{{ __('generic.car_vin_and_number') }}</label>
                                            <input type="text" class="form-control" id="car_number" name="car_number" placeholder=""
                                                   value="{{ old('car_number') }}" required>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="order_number" value="">
                                <input type="hidden" name="warranty_template" value="">

                                <div class="alert alert-success" style="display: none"></div>
                                <div class="alert-system"></div>
                                <button type="submit" class="btn btn-primary pull-right save" id="submitFormAddNewCardAjax">
                                    <span class="spiner"></span>
                                    {{ __('voyager::generic.add') }}
                                </button>
                            </form>
                        </div>


                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

@push('javascript')
    <script>
        $(document).ready(function () {

            // Modal
            (function () {
                const modal = document.getElementById('addNewAgentCardModal');
                if ( !modal )
                    return;

                const orderNumberInput = modal.querySelector('input[name="order_number"]');
                const warrantyTemplateInput = modal.querySelector('input[name="warranty_template"]');
                const metersInput = modal.querySelector('input[name="meters"]');
                const titleWarrantyTemplate = document.getElementById('titleWarrantyTemplate');
                const titleOrderNumber = document.getElementById('titleOrderNumber');

                if ( !orderNumberInput || !warrantyTemplateInput || !titleWarrantyTemplate || !titleOrderNumber || !metersInput )
                    return;

                // Event Listeners for buttons
                var buttonsModal = document.querySelectorAll('.btn-add-new-card');
                if ( buttonsModal.length > 0 ) {
                    buttonsModal.forEach( function (el) {
                        el.addEventListener('click', function(e) {
                            e.preventDefault();
                            orderNumberInput.value = e.target.dataset.orderNumber;
                            warrantyTemplateInput.value = e.target.dataset.warrantyTemplate;
                            metersInput.max = e.target.dataset.orderMetersLeft;
                            titleOrderNumber.firstElementChild.innerHTML = e.target.dataset.orderNumber;
                            titleWarrantyTemplate.firstElementChild.innerHTML = e.target.dataset.warrantyTemplate;
                            if ( orderNumberInput.value
                                && warrantyTemplateInput.value
                                && titleOrderNumber.firstElementChild.innerHTML
                                && titleWarrantyTemplate.firstElementChild.innerHTML
                            ) {
                                $(modal).modal('show');
                            }
                        });
                    });
                }

                $(modal).on('shown.bs.modal', function () {

                });

                $(modal).on('hidden.bs.modal', function () {
                    orderNumberInput.value = '';
                    warrantyTemplateInput.value = '';
                    titleOrderNumber.firstElementChild.innerHTML = '';
                    titleWarrantyTemplate.firstElementChild.innerHTML = '';
                });
            })();


            const validatePhone = function (strNumber) {
                var phone_pattern = {{config('services.nexmo.phone_number_pattern')}};
                if( strNumber.match(phone_pattern) ) {
                    return true;
                } else {
                    return false;
                }
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Define variables
            const form = document.getElementById('formAddNewAgentCardAjax');
            const formData = form.elements;
            const submitButton = form.querySelector('button[type="submit"]');

            // Validation phone field
            formData['phone_number'].addEventListener('keyup', (e) => {
                let validationPhoneField = e.target.parentElement.querySelector('.validation-input-msg');
                if (!validationPhoneField) return;

                validationPhoneField.innerHTML = '';
                validationPhoneField.classList.remove('success', 'error');
                submitButton.classList.remove('disabled');
                if (validatePhone(e.target.value)) {
                    validationPhoneField.innerHTML = 'Верный формат';
                    validationPhoneField.classList.add('success');
                    submitButton.classList.remove('disabled');
                } else {
                    validationPhoneField.innerHTML = 'Не валидный номмер, попробуйте формат +77XXXXXXX';
                    validationPhoneField.classList.add('error');
                    submitButton.classList.add('disabled');
                }
            });

            // On submit form
            form.addEventListener('submit', (e) => {
                e.preventDefault();
                let _this = e.target;

                let alert = _this.querySelector('.alert');
                let alertSystem = _this.querySelector('.alert-system');
                data = {
                    full_name: formData['full_name'].value,
                    master: formData['master'].value,
                    phone_number: formData['phone_number'].value,
                    car_brand_model: formData['car_brand_model'].value,
                    car_number: formData['car_number'].value,
                    warranty_template: formData['warranty_template'].value,
                    order_number: formData['order_number'].value,
                    meters: formData['meters'].value,
                };

                console.log(data)
                $.ajax({
                    type:'POST',
                    url: '{{route('agent.cards.store_ajax')}}',
                    data: data,
                    beforeSend: function( xhr ) {
                        submitButton.classList.add('loading');
                    },
                    success: function(data) {
                        submitButton.classList.remove('loading');
                        $(submitButton).hide();
                        console.log(data);
                        if (data.success ?? data.card) {
                            console.log(data);
                            console.log(data.success);

                            alert.classList.add('alert-success');
                            alert.innerHTML = (data.success && data.card) ? 'Карта <strong>' + data.card + '</strong> успешно создана.' : '';
                            $(alert).show();
                            alertSystem.innerHTML = '<string>Подождите, идет перезагрузка страницы ...</string>';
                            $(alertSystem).show();
                            setTimeout(() => {
                                document.location.reload();
                            }, 2000);

                        }
                        if (data.error) {
                            console.log(data.error);
                            alert.classList.add('alert-danger');
                            alert.innerHTML = data.error;
                            $(alert).show();
                            setTimeout(function () {
                                let alertDanger = _this.querySelector('.alert-danger');
                                alertDanger.classList.remove('alert-danger');
                                $(alertDanger).hide();
                                alertDanger.innerHTML = '';
                                $('#addNewAgentCardModal').modal('hide');
                            }, 2000);
                        }
                    }
                });
            });

        });
    </script>
@endpush
