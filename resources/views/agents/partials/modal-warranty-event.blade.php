<div class="modal modal-info fade" tabindex="-1" id="registerWarrantyEvent" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h4 class="modal-title"><i class="voyager-certificate"></i>
                        {{ __('generic.register_warranty_event') }}
                    </h4>
                    <h5 class="text-primary">
                        {{ __('generic.warranty_card') }}: <strong class="header-number"></strong>
                    </h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-edit-add" role="form" >
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="panel-body">
                        <form action="">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="warranty_event">{{ __('generic.warranty_event') }}</label>
                                <textarea type="text" class="form-control" id="warranty_event" name="warranty_event" required>{{ old('warranty_event') }}</textarea>
                            </div>

                            <input type="hidden" name="number" value="">

                            <div class="alert alert-success" style="display: none"></div>
                            <div class="alert-system text-info" style="display: none"></div>

                            <button type="submit" class="btn btn-primary pull-right save" id="submitFormAddNewCardAjax">
                                <span class="spiner"></span>
                                {{ __('voyager::generic.add') }}
                            </button>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

@push('javascript')
    <script>
        $(document).ready(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Define variables
            var modal = document.getElementById('registerWarrantyEvent');
            var form = modal.querySelector('form');
            var warrantyEventField = form.querySelector('textarea[name=warranty_event]');
            var numberField = form.querySelector('input[name=number]');
            var headerNumber = modal.querySelector('.header-number');

            // Event Listeners for buttons
            var buttonsModal = document.querySelectorAll('.btn-register-warranty-event');
            if (buttonsModal.length > 0) {
                buttonsModal.forEach(function (el) {
                    el.addEventListener('click', function(e) {
                        e.preventDefault();
                        $(modal).modal('show');
                        numberField.value = '';
                        numberField.value = $(this).data('number');
                        headerNumber.innerText = '';
                        headerNumber.innerText = $(this).data('number');

                    });
                })
            }

            // On submit form
            $(form).on('submit', function(e){
                e.preventDefault();

                var this_this = $(this);
                var sumbit_button = $(this).find('button[type="submit"]');

                var warrantyEvent = warrantyEventField.value;
                var number = numberField.value;

                data = {
                    warranty_event: warrantyEvent,
                    number: number,
                };

                $.ajax({
                    type:'POST',
                    url: '{{route('agent.cards.update_event_ajax')}}',
                    data: data,
                    beforeSend: function( xhr ) {
                        sumbit_button.addClass('loading');
                    },
                    success: function(data) {
                        sumbit_button.removeClass('loading');
                        console.log(data);
                        if (data.success ?? data.events) {
                            console.log(data.success);
                            sumbit_button.hide();
                            this_this.find('.alert').addClass('alert-success').html((data.success && data.events) ? '<h5>Регистрация гарантии успешно добавлена</h5> <strong>' + data.events + '</strong>' : '').show();
                            this_this.find('.alert-system').html('<string>Подождите, идет перезагрузка страницы ...</string>').show();
                            setTimeout(function () {
                                document.location.reload();
                            }, 2000);

                        }
                        if (data.error) {
                            console.log(data.error);
                            this_this.find('.alert').addClass('alert-danger').text(data.error).show();
                            setTimeout(function () {
                                this_this.find('.alert-danger').removeClass('alert-danger').hide().text('');
                                $('#addNewCardModal').modal('hide');
                            }, 2000);
                        }
                    }
                });
            });

        });

    </script>
@endpush
