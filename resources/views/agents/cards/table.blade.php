@if (count($cards) > 0)
    <div class="agent-cards-list">
        <div class="agent-cards-list__head">
            <div class="agent-cards-list__head--index">#</div>
            <div class="agent-cards-list__head--number">{{__('generic.number')}}</div>
        </div>
        <div class="agent-cards-list__body">
            @foreach($cards as $k => $card)
                <div class="agent-cards-list__body-row">
                    <div class="agent-cards-list__body-row-visible">
                        <div class="agent-cards-list__body-row-visible--index">
                            {{$k+1}}
                        </div>
                        <div class="agent-cards-list__body-row-visible--number">
                            {{$card->number}}
                        </div>
                        <div class="agent-cards-list__body-row-visible--action">
                            <button data-number="{{$card->number}}">
                                <i class="fa fa-angle-down"></i>
                            </button>
                        </div>
                    </div>
                    <div class="agent-cards-list__body-row-toggle">
                        <div class="agent-cards-list__body-row-toggle-in">
                            <span class="load"><i class="fas fa-circle-notch fa-spin"></i></span>
                            <ul class="agent-card-fields"></ul>
                            <div class="agent-card-nav">
                                <a href="#" data-number="{{$card->number}}"
                                   class="btn btn-sm btn-primary desctable btn-register-warranty-event">
                                    {{ __('generic.register_warranty_event') }}
                                </a>
                                <a href="#" data-number="{{$card->number}}" class="btn btn-secondary btn-show-qr-code">
                                    <img src="{{asset('storage')}}/qr.svg" alt="">
                                </a>
                                <a href="#" data-number="{{$card->number}}" class="btn btn-info btn-send-sms">
                                    <span class="spiner"></span>
                                    <img src="{{asset('storage')}}/sms.svg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
        <div class="agent-cards-pagination">
            {{ $cards->links() }}
        </div>
    </div>
@else
    <div class="div alert alert-warning">{{ __('generic.you_have_not_warranty_cards') }}</div>
@endif
@include('agents/partials/modal-warranty-qr-code')
@push('javascript')
    <script>
        (function() {
            const toggleButtons = document.querySelectorAll('.agent-cards-list__body-row-visible--action button');

            if (!toggleButtons.length) return;

            toggleButtons.forEach( function(el) {
                el.addEventListener('click', function(e) {
                    e.preventDefault();
                    const row = e.target.closest('.agent-cards-list__body-row');
                    const toggle = row.querySelector('.agent-cards-list__body-row-toggle');
                    [...row.parentNode.children].forEach(function (el) {
                        el.classList.remove('toggled');
                        el.querySelector('.agent-cards-list__body-row-toggle').style.height = '';
                    });
                    if ( row.classList.contains('toggled') ) {
                        row.classList.remove('toggled');
                        toggle.style.height = '';
                    } else {
                        row.classList.add('toggled');
                        toggle.style.height = toggle.children[0].offsetHeight + 'px';
                        getDataAgentWarrantyCard(e.target);
                    }

                })
            })

            const getDataAgentWarrantyCard = function (el) {
                const number = el.dataset.number;
                const row = el.closest('.agent-cards-list__body-row');
                const rows = document.querySelectorAll('.agent-cards-list__body-row')
                const toggle = row.querySelector('.agent-cards-list__body-row-toggle')
                const load = row.querySelector('.load');
                const for_fields = row.querySelector('.agent-card-fields');
                if (!load) return;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('agent.cards.get_card_ajax') }}',
                    data: { number: number },
                    beforeSend: function( xhr ) {
                        row.classList.add('loading');
                        rows.forEach(function (el) {
                            el.style.pointerEvents = 'none';
                        });
                    },
                    success: function(data) {
                        toggle.style.height = toggle.children[0].offsetHeight + 'px';
                        row.classList.remove('loading');
                        rows.forEach(function (el) {
                            el.style.pointerEvents = '';
                        });
                        if ( data.success ) {
                            let html = '';
                            for (const key in data.fields) {
                                html += `<li><span>${key}:</span><span></span><span>${data.fields[key]}</span></li>`;
                            }
                            console.log(load)
                            load.remove();
                            for_fields.innerHTML = html;
                            toggle.style.height = toggle.children[0].offsetHeight + 'px';
                        }
                        if ( data.error ) {
                            console.log(data.error);
                        }
                    }
                });
            }
        })();

        (function () {
            // Send warranty sms
            let smsButtons = document.querySelectorAll('.btn-send-sms');
            if (smsButtons.length > 0) {
                smsButtons.forEach(function (el) {
                    el.addEventListener('click', function (e) {
                        e.preventDefault();
                        var this_this = this;
                        if ( confirm("Вы подтверждаете отправку SMS") ) {
                            $.ajax({
                                type:'POST',
                                url: '{{ route('agent.cards.send_sms_ajax') }}',
                                data: { number: this.dataset.number },
                                beforeSend: function( xhr ) {
                                    this_this.classList.add('loading');
                                },
                                success: function(data) {
                                    this_this.classList.remove('loading');
                                    const alert = document.createElement('span');
                                    alert.innerHTML = data.msg;
                                    this_this.after(alert);
                                    if (data.success) {
                                        alert.classList.add('text-success');
                                    } else {
                                        alert.classList.add('text-danger');
                                    }
                                    setTimeout(function () {
                                        alert.remove();
                                    }, 5000);
                                }
                            });
                        }
                    });
                });
            }
        })();
    </script>
@endpush
