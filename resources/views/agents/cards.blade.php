@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="title-form-h4">{{__('generic.warranty_cards')}}</h1>
        @if ($is_approved)
            @include('agents.cards.table', $cards)
        @else
            <div class="div alert alert-danger">{{ __('generic.your_account_is_not_approved_yet') }}</div>
        @endif
    </div>
@endsection

@include('agents.partials.modal-warranty-event')
