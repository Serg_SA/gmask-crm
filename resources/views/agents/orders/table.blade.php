@if (count($orders) > 0)
    <div class="table-responsive">
        <table class="table table-hover table-agent-orders">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">{{__('generic.number')}}</th>
                <th scope="col">{{__('generic.warranty_template')}}</th>
                <th scope="col">{{__('generic.linear_meters_quantity')}}</th>
                <th scope="col">{{__('generic.linear_meters_left_quantity')}}</th>
                <th scope="col" class="text-right">{{__('voyager::generic.actions')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $k => $order)
                <tr @if ($order->meters_left < 1) disabled @endif>
                    <th scope="row">{{$k+1}}</th>
                    <td>{{$order->number}}</td>
                    <td>{{$order->warranty_template}}</td>
                    <td class="text-center">{{$order->meters}}</td>
                    <td class="text-center">{{$order->meters_left}}</td>
                    <td class="text-right">
                    <span class="pull-right">
                        <a href="#" class="btn btn-sm btn-primary desctable btn-add-new-card"
                           data-order-number="{{$order->number}}"
                           data-warranty-template="{{$order->warranty_template}}"
                           data-order-meters-left="{{$order->meters_left}}">
                            {{ __('generic.issue_warranty') }}
                        </a>
                    </span>
                        {{--<a href="{{ route('admin.car_user.edit', $order->pivot->id) }}"--}}
                        {{--class="btn btn-sm btn-primary pull-right desctable" style="display:inline;">--}}
                        {{--<i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}--}}
                        {{--</a>--}}
                        {{--<a href="{{ route('admin.car_user.show', $order->pivot->id) }}"--}}
                        {{--class="btn btn-sm btn-warning pull-right desctable" style="display:inline; margin-right:5px;">--}}
                        {{--<i class="voyager-eye"></i> {{ __('voyager::generic.view') }}--}}
                        {{--</a>--}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@else
    <div class="div alert alert-warning">{{ __('generic.you_have_not_orders') }}</div>
@endif
