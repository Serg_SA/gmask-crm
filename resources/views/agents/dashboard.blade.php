@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="title-form-h4">{{__('generic.your_orders')}}</h1>
        @if ($is_approved)
            @include('agents.orders.table')
        @else
            <div class="div alert alert-danger">{{ __('generic.your_account_is_not_approved_yet') }}</div>
        @endif
    </div>
@endsection

@include('agents.partials.modal-add-card')
