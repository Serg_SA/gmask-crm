<input type="datetime-local" class="form-control" name="{{ $row->field }}"
       placeholder="{{ $row->getTranslatedAttribute('display_name') }}"
       value="@if(isset($dataTypeContent->{$row->field})){{\Carbon\Carbon::parse(old($row->field, $dataTypeContent->{$row->field}))->format('Y-m-d\TH:i:s')}}@else{{old($row->field)}}@endif">
