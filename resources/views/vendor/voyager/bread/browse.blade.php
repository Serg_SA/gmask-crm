@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->getTranslatedAttribute('display_name_plural'))

@section('page_header')

    <div class="container-fluid">
        <!-- Custom -->
        @if ($dataType->model_name === 'App\Order')
            @inject('order_service', 'App\Services\OrderService')
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <button class="btn btn-primary btn-calendar-close collapsed" type="button" data-toggle="collapse" data-target="#collapseCalendar" aria-expanded="false" aria-controls="collapseCalendar">
                            {{__('voyager::generic.close')}} {{__('generic.calendar')}}
                        </button>
                        <button class="btn btn-primary btn-calendar-open collapsed" type="button" data-toggle="collapse" data-target="#collapseCalendar" aria-expanded="false" aria-controls="collapseCalendar">
                            {{__('voyager::generic.open')}} {{__('generic.calendar')}}
                        </button>
                    </p>
                    <div class="collapse" id="collapseCalendar">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>
        @endif
        <!-- End Custom -->
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->getTranslatedAttribute('display_name_plural') }}
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
        @can('delete', app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit', app($dataType->model_name))
            @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary btn-add-new">
                    <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                </a>
            @endif
        @endcan
        @can('delete', app($dataType->model_name))
            @if($usesSoftDeletes)
                <input type="checkbox" @if ($showSoftDeleted) checked @endif id="show_soft_deletes" data-toggle="toggle" data-on="{{ __('voyager::bread.soft_deletes_off') }}" data-off="{{ __('voyager::bread.soft_deletes_on') }}">
            @endif
        @endcan
        @foreach($actions as $action)
            @if (method_exists($action, 'massAction'))
                @include('voyager::bread.partials.actions', ['action' => $action, 'data' => null])
            @endif
        @endforeach
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if ($isServerSide)
                            <form method="get" class="form-search">
                                <div id="search-input">
                                    <div class="col-2">
                                        <select id="search_key" name="key">
                                            @foreach($searchNames as $key => $name)
                                                <option value="{{ $key }}" @if($search->key == $key || (empty($search->key) && $key == $defaultSearchKey)) selected @endif>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-2">
                                        <select id="filter" name="filter">
                                            <option value="contains" @if($search->filter == "contains") selected @endif>contains</option>
                                            <option value="equals" @if($search->filter == "equals") selected @endif>=</option>
                                        </select>
                                    </div>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="{{ __('voyager::generic.search') }}" name="s" value="{{ $search->value }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-lg" type="submit">
                                                <i class="voyager-search"></i>
                                            </button>
                                        </span>
                                        <div id="dropdownMenu" class="dropdown-menu">
                                            <a class="dropdown-item" href="#" data-value="in_process">В процессе</a>
                                            <a class="dropdown-item" href="#" data-value="completed">Готово</a>
                                            <a class="dropdown-item" href="#" data-value="completed_not_happy">Готово (не доволен)</a>
                                            <a class="dropdown-item" href="#" data-value="released">Выдан</a>
                                            <a class="dropdown-item" href="#" data-value="released_not_happy">Выдан (не доволен)</a>
                                        </div>
                                    </div>
                                </div>
                                @if (Request::has('sort_order') && Request::has('order_by'))
                                    <input type="hidden" name="sort_order" value="{{ Request::get('sort_order') }}">
                                    <input type="hidden" name="order_by" value="{{ Request::get('order_by') }}">
                                @endif
                            </form>
                        @endif
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover {{-- Custom --}}table-{{$dataType->name}} {{-- Custom End --}}">
                                <thead>
                                    <tr>
                                        @if($showCheckboxColumn)
                                            <th>
                                                <input type="checkbox" class="select_all">
                                            </th>
                                        @endif
                                        @foreach($dataType->browseRows as $row)
                                        <th>
                                            @if ($isServerSide)
                                                <a href="{{ $row->sortByUrl($orderBy, $sortOrder) }}">
                                            @endif
                                            {{ $row->getTranslatedAttribute('display_name') }}
                                            @if ($isServerSide)
                                                @if ($row->isCurrentSortField($orderBy))
                                                    @if ($sortOrder == 'asc')
                                                        <i class="voyager-angle-up pull-right"></i>
                                                    @else
                                                        <i class="voyager-angle-down pull-right"></i>
                                                    @endif
                                                @endif
                                                </a>
                                            @endif
                                        </th>
                                        @endforeach
                                        <!-- Custom -->
                                        @if ($dataType->model_name == 'App\Order')
                                            <th>{{__('generic.total_price')}}</th>
                                        @endif
                                        <!-- End Custom -->
                                        <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataTypeContent as $data)
                                    <tr>
                                        @if($showCheckboxColumn)
                                            <td>
                                                <input type="checkbox" name="row_id" id="checkbox_{{ $data->getKey() }}" value="{{ $data->getKey() }}">
                                            </td>
                                        @endif
                                        @foreach($dataType->browseRows as $row)
                                            @php
                                            if ($data->{$row->field.'_browse'}) {
                                                $data->{$row->field} = $data->{$row->field.'_browse'};
                                            }
                                            @endphp
                                            <td>
                                                @if (isset($row->details->view))
                                                    @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $data->{$row->field}, 'action' => 'browse', 'view' => 'browse', 'options' => $row->details])
                                                @elseif($row->type == 'image')
                                                    <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:100px">
                                                @elseif($row->type == 'relationship')
                                                    @include('voyager::formfields.relationship', ['view' => 'browse','options' => $row->details])
                                                @elseif($row->type == 'select_multiple')
                                                    @if(property_exists($row->details, 'relationship'))

                                                        @foreach($data->{$row->field} as $item)
                                                            {{ $item->{$row->field} }}
                                                        @endforeach

                                                    @elseif(property_exists($row->details, 'options'))
                                                        @if (!empty(json_decode($data->{$row->field})))
                                                            @foreach(json_decode($data->{$row->field}) as $item)
                                                                @if (@$row->details->options->{$item})
                                                                    {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            {{ __('voyager::generic.none') }}
                                                        @endif
                                                    @endif

                                                    @elseif($row->type == 'multiple_checkbox' && property_exists($row->details, 'options'))
                                                        @if (@count(json_decode($data->{$row->field})) > 0)
                                                            @foreach(json_decode($data->{$row->field}) as $item)
                                                                @if (@$row->details->options->{$item})
                                                                    {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            {{ __('voyager::generic.none') }}
                                                        @endif

                                                @elseif(($row->type == 'select_dropdown' || $row->type == 'radio_btn') && property_exists($row->details, 'options'))
                                                    <!-- Custom -->
                                                    @if ($dataType->model_name == 'App\Order' && $row->field == 'status')
                                                        <div class="order_status">
                                                            <input type="checkbox" class="order_edit_status" id="orderEditStatus{{$data->getKey()}}">
                                                            <span class="order_status_label" data-status-label="{{ $row->details->options->{ $data->{$row->field} } ?? '' }}">{{ $row->details->options->{ $data->{$row->field} } ?? '' }}</span>
                                                            @if ($order_service->isGuaranteed($data))
                                                                @if (Auth::user()->role->name === 'admin' || (Auth::user()->role !== 'admin'))
                                                                    <label for="orderEditStatus{{$data->getKey()}}">
                                                                        <i class="icon voyager-pen" title="{{__('voyager::generic.edit')}}"></i>
                                                                        <i class="icon voyager-x" title="{{__('voyager::generic.close')}}"></i>
                                                                    </label>
                                                                    <select name="order_status" data-order-id="{{$data->getKey()}}">
                                                                        @if($row->details->options)
                                                                            @foreach($row->details->options as $option_key => $option_label)
                                                                                @php $disabled = '' @endphp
                                                                                @if ((Auth::user()->role->name === 'admin'))
                                                                                    <option value="{{$option_key}}" {{$data->status == $option_key ? 'selected' : ''}} {{$data->status == ''}}>{{$option_label}}</option>
                                                                                @else
                                                                                    @if ($data->status === 'in_process')
                                                                                        @if(in_array($option_key, ['in_process']))
                                                                                            @php $disabled = 'disabled' @endphp
                                                                                        @endif
                                                                                    @elseif(in_array($data->status, ['completed', 'completed_not_happy']))
                                                                                        @if(in_array($option_key, ['in_process']))
                                                                                            @php $disabled = 'disabled' @endphp
                                                                                        @endif
                                                                                    @elseif(in_array($data->status, ['released', 'released_not_happy']))
                                                                                        @if(in_array($option_key, ['in_process', 'completed', 'completed_not_happy']))
                                                                                            @php $disabled = 'disabled' @endphp
                                                                                        @endif
                                                                                    @endif
                                                                                    <option value="{{$option_key}}" {{$data->status == $option_key ? 'selected' : ''}} {{$data->status == ''}} {{$disabled}}>{{$option_label}}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                @endif
                                                            @endif
                                                        </div>
                                                    @else
                                                        {!! $row->details->options->{$data->{$row->field}} ?? '' !!}
                                                    @endif
                                                    <!-- End Custom -->
                                                @elseif($row->type == 'date' || $row->type == 'timestamp')
                                                    @if ( property_exists($row->details, 'format') && !is_null($data->{$row->field}) )
                                                        {{ \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($row->details->format) }}
                                                    @else
                                                        {{ $data->{$row->field} }}
                                                    @endif
                                                @elseif($row->type == 'checkbox')
                                                    <!-- Custom -->
                                                    @if ($dataType->model_name == 'App\Agent' && $row->field == 'is_approved')
                                                        <label class="switch-toggle" title="Вкл / Выкл">
                                                            <input type="checkbox" name="agent_approved" @if($data->{$row->field}) checked @endif data-id="{{$data->getKey()}}">
                                                            <span class="slider round"></span>
                                                        </label>
                                                        <span class="switch-toggle__alert"></span>
                                                    @else
                                                    <!-- End Custom -->
                                                        @if(property_exists($row->details, 'on') && property_exists($row->details, 'off'))
                                                            @if($data->{$row->field})
                                                                <span class="label label-info">{{ $row->details->on }}</span>
                                                            @else
                                                                <span class="label label-primary">{{ $row->details->off }}</span>
                                                            @endif
                                                        @else
                                                            {{ $data->{$row->field} }}
                                                        @endif
                                                    <!-- Custom -->
                                                    @endif
                                                    <!-- End Custom -->
                                                @elseif($row->type == 'color')
                                                    <span class="badge badge-lg" style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                                                @elseif($row->type == 'text')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div>{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'text_area')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div>{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    @if(json_decode($data->{$row->field}) !== null)
                                                        @foreach(json_decode($data->{$row->field}) as $file)
                                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}" target="_blank">
                                                                {{ $file->original_name ?: '' }}
                                                            </a>
                                                            <br/>
                                                        @endforeach
                                                    @else
                                                        <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($data->{$row->field}) }}" target="_blank">
                                                            Download
                                                        </a>
                                                    @endif
                                                @elseif($row->type == 'rich_text_box')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div>{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                                                @elseif($row->type == 'coordinates')
                                                    @include('voyager::partials.coordinates-static-image')
                                                @elseif($row->type == 'multiple_images')
                                                    @php $images = json_decode($data->{$row->field}); @endphp
                                                    @if($images)
                                                        @php $images = array_slice($images, 0, 3); @endphp
                                                        @foreach($images as $image)
                                                            <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:50px">
                                                        @endforeach
                                                    @endif
                                                @elseif($row->type == 'media_picker')
                                                    @php
                                                        if (is_array($data->{$row->field})) {
                                                            $files = $data->{$row->field};
                                                        } else {
                                                            $files = json_decode($data->{$row->field});
                                                        }
                                                    @endphp
                                                    @if ($files)
                                                        @if (property_exists($row->details, 'show_as_images') && $row->details->show_as_images)
                                                            @foreach (array_slice($files, 0, 3) as $file)
                                                            <img src="@if( !filter_var($file, FILTER_VALIDATE_URL)){{ Voyager::image( $file ) }}@else{{ $file }}@endif" style="width:50px">
                                                            @endforeach
                                                        @else
                                                            <ul>
                                                            @foreach (array_slice($files, 0, 3) as $file)
                                                                <li>{{ $file }}</li>
                                                            @endforeach
                                                            </ul>
                                                        @endif
                                                        @if (count($files) > 3)
                                                            {{ __('voyager::media.files_more', ['count' => (count($files) - 3)]) }}
                                                        @endif
                                                    @elseif (is_array($files) && count($files) == 0)
                                                        {{ trans_choice('voyager::media.files', 0) }}
                                                    @elseif ($data->{$row->field} != '')
                                                        @if (property_exists($row->details, 'show_as_images') && $row->details->show_as_images)
                                                            <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:50px">
                                                        @else
                                                            {{ $data->{$row->field} }}
                                                        @endif
                                                    @else
                                                        {{ trans_choice('voyager::media.files', 0) }}
                                                    @endif
                                                @else
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <span>{{ $data->{$row->field} }}</span>
                                                @endif
                                                <!-- Custom -->
                                                @if ($dataType->model_name == 'App\Order')
                                                    @if (isset($row->field) && $row->field == 'order_belongsto_user_relationship')
                                                        {{ $data->user->name ?? '' }}
                                                    @endif

                                                    @if (isset($row->field) && $row->field == 'name')
                                                        @if ($data->items->count() > 0)
                                                            <div class="order__items-wr">
                                                                <div class="order__services-title">{{__('generic.services')}}:</div>
                                                                <ol class="order__items">
                                                                    @foreach($data->items as $item)
                                                                        <li>
                                                                            <small>
                                                                                {{$item->service->name ?? ''}}
                                                                                @if ( $item->service->cardTemplate )

                                                                                    @if ($item->card)
                                                                                        @include('partials/warranty-card', ['card' => $item->card])
                                                                                    @else
                                                                                        <a href="#" class="btn-add-new-card" data-item="{{$item->id}}" data-template="{{$item->service->cardTemplate->name ?? ''}}">
                                                                                            {{ __('generic.issue_warranty') }}
                                                                                        </a>
                                                                                    @endif
                                                                                @endif
                                                                            </small>
                                                                        </li>
                                                                    @endforeach
                                                                </ol>
                                                            </div>
                                                        @endif
                                                    @endif
                                                @endif
                                                <!-- End Custom -->
                                            </td>
                                        @endforeach
                                        <!-- Custom -->
                                        @if ($dataType->model_name == 'App\Order')
                                            <td>
                                                {{$order_service->getTotalPrice($data->getKey())}}
                                            </td>
                                        @endif
                                        <!-- End Custom -->
                                        <td class="no-sort no-click bread-actions">
                                            @foreach($actions as $action)
                                                @if (!method_exists($action, 'massAction'))
                                                    @include('voyager::bread.partials.actions', ['action' => $action])
                                                @endif
                                            @endforeach
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if ($isServerSide)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                                    'voyager::generic.showing_entries', $dataTypeContent->total(), [
                                        'from' => $dataTypeContent->firstItem(),
                                        'to' => $dataTypeContent->lastItem(),
                                        'all' => $dataTypeContent->total()
                                    ]) }}</div>
                            </div>
                            <div class="pull-right">
                                {{ $dataTypeContent->appends([
                                    's' => $search->value,
                                    'filter' => $search->filter,
                                    'key' => $search->key,
                                    'order_by' => $orderBy,
                                    'sort_order' => $sortOrder,
                                    'showSoftDeleted' => $showSoftDeleted,
                                ])->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Custom -->
    @include('partials.modal-add-card')
    @include('partials.modal-warranty-event')
    @include('partials.modal-warranty-qr-code')
    <!-- End Custom -->
@stop

@section('css')
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
    <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
<!-- Custom -->
@if ($dataType->model_name === 'App\Order')
    <link href="{{ asset('js/libs/fullcalendar/main.css') }}" rel='stylesheet' />
    <link href='https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.13.1/css/all.css' rel='stylesheet'>
@endif
<!-- End Custom -->
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
        $(document).ready(function () {
            @if (!$dataType->server_side)
                var table = $('#dataTable').DataTable({!! json_encode(
                    array_merge([
                        "order" => $orderColumn,
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [['targets' => -1, 'searchable' =>  false, 'orderable' => false]],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});
            @else
                $('#search-input select').select2({
                    minimumResultsForSearch: Infinity
                });
            @endif

            @if ($isModelTranslatable)
                $('.side-body').multilingual();
                //Reinitialise the multilingual features when they change tab
                $('#dataTable').on('draw.dt', function(){
                    $('.side-body').data('multilingual').init();
                })
            @endif
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
            });

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', '__id') }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        @if($usesSoftDeletes)
            @php
                $params = [
                    's' => $search->value,
                    'filter' => $search->filter,
                    'key' => $search->key,
                    'order_by' => $orderBy,
                    'sort_order' => $sortOrder,
                ];
            @endphp
            $(function() {
                $('#show_soft_deletes').change(function() {
                    if ($(this).prop('checked')) {
                        $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 1]), true)) }}"></a>');
                    }else{
                        $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 0]), true)) }}"></a>');
                    }

                    $('#redir')[0].click();
                })
            })
        @endif
        $('input[name="row_id"]').on('change', function () {
            var ids = [];
            $('input[name="row_id"]').each(function() {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            $('.selected_ids').val(ids);
        });
    </script>

    <!-- Custom -->
    @if ($dataType->model_name === 'App\Order')
    <script src="{{asset('js/libs/fullcalendar/main.js') }}"></script>
    <script src="{{asset('js/libs/fullcalendar/locales/ru.js') }}"></script>
    <script>
        window.eventsDataJson = <?php echo json_encode(App\Facades\Order::getFullcalendarData()) ?>;
        document.addEventListener('DOMContentLoaded', function() {
            // console.log(window.eventsDataJson);
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay'
                },
                locale: 'ru',
                themeSystem: 'bootstrap',
                events: JSON.parse(window.eventsDataJson),
            });
            calendar.render();
        });
        $(document).ready(function () {
          $('#collapseCalendar').on('show.bs.collapse', function () {
            window.dispatchEvent(new Event('resize'));
          });
        });

        $(document).ready(function () {
            // Order status
            $('select[name=order_status]').change(function () {
               var this_this = $(this);
               var newStatus = $(this).val();
               var newStatusLabel = $(this).find('option:selected').text();
                params = {
                    order_id: $(this).data('order-id'),
                    status: newStatus,
                    status_label: newStatusLabel,
                    _token: '{{ csrf_token() }}',
                }
                $.post('{{ route('admin.orders.status') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {
                        toastr.success(response.data.message);
                        this_this.siblings('input[type="checkbox"]').prop( "checked", false );
                        this_this.siblings('.order_status_label').attr('data-status-label', response.data.status_label).text(response.data.status_label);
                    } else {
                        toastr.error("Error updating status.");
                    }
                });
            });

            // View warranty card
            let cards = document.querySelectorAll('.order__items .card');

            if (cards.length > 0) {
                cards.forEach(function (el) {
                    let button = el.querySelector('.button-toggle');

                    button.addEventListener('click', function () {
                       this.parentNode.classList.toggle('toggled')
                    });
                });

            }

            // Send warranty sms
            let smsButtons = document.querySelectorAll('.btn-send-sms');
            if (smsButtons.length > 0) {
                smsButtons.forEach(function (el) {
                    el.addEventListener('click', function (e) {
                        e.preventDefault();
                        var this_this = this;
                        var card_id = this_this.dataset.card_id;
                        if ( confirm("Вы подтверждаете отправку SMS") ) {
                            data = {
                                card_id: card_id,
                            };

                            $.ajax({
                                type:'POST',
                                url: '{{route('admin.cards.send_sms_ajax')}}',
                                data: data,
                                beforeSend: function( xhr ) {
                                    this_this.classList.add('loading');
                                },
                                success: function(data) {
                                    if (data.success) {
                                        this_this.classList.remove('loading');
                                        this_this.classList.add('sent');
                                        this_this.setAttribute('data-sent_sms', Number(this_this.dataset.sent_sms) + 1);
                                    } else {

                                    }
                                }
                            });
                        }
                    });
                });
            }
        });

        // Search for statuses
        $(document).ready(function() {
          var selectedFilterValue = $("#search_key").val();
          $("#search_key").change(function() {
            $("input[name='s']").val('');
            selectedFilterValue = $(this).val();
          });

          $("input[name='s']").click(function(e) {
            if (selectedFilterValue !== 'status')
              return;
            $('#dropdownMenu').toggleClass('show');
          });

          // Insert selected value into input field
          $('#dropdownMenu').on('click', 'a', function(e) {
            e.preventDefault();
            var selectedValue = $(this).data('value');
            $("input[name='s']").val(selectedValue);
            $('#dropdownMenu').removeClass('show');
          });

          // Close dropdown menu when clicking outside of it
          $(document).click(function(event) {
            if (!$(event.target).closest("input[name='s']").length) {
              $('#dropdownMenu').removeClass('show');
            }
          });

        });
    </script>
    @endif
    @if ($dataType->model_name === 'App\Agent')
        <script>
            (function() {
                const agentToggles = document.querySelectorAll('input[name="agent_approved"]');
                if (!agentToggles.length) return;
                agentToggles.forEach(function(el) {
                    el.addEventListener('change', function (e) {
                        const toggle = e.target.closest('.switch-toggle');
                        let isApproved;
                        let agentId = e.target.dataset.id;
                        if (e.target.checked) {
                            isApproved = 1;
                        } else {
                            isApproved = 0;
                        }
                        data = {
                            agent_id : agentId,
                            is_approved : isApproved
                        };
                        $.ajax({
                            type:'POST',
                            url: '{{route('admin.agents.approve_ajax')}}',
                            data: data,
                            beforeSend: function( xhr ) {
                                toggle.style.pointerEvents = 'none';
                            },
                            success: function(data) {
                                if (data.success) {
                                    toggle.nextElementSibling.classList.add('success');
                                    toggle.nextElementSibling.innerHTML = 'Обновлено';
                                } else {
                                    toggle.nextElementSibling.classList.add('danger');
                                    toggle.nextElementSibling.innerHTML = 'Не обновлено';
                                }
                                setTimeout(function() {
                                    toggle.nextElementSibling.innerHTML = '';
                                    toggle.nextElementSibling.classList.remove();
                                    toggle.style.pointerEvents = '';
                                }, 3000);
                            }
                        });
                    });
                });
            })();
        </script>
    @endif
    <!-- End Custom -->
@stop
