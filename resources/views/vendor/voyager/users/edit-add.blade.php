@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">


            {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('admin.users.verify') }}">--}}
                {{--{{ csrf_field() }}--}}

                {{--<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">--}}
                    {{--<label for="code" class="col-md-4 control-label">Code</label>--}}

                    {{--<div class="col-md-6">--}}
                        {{--<input id="code" type="number" class="form-control" name="code" value="{{ old('code') }}" required autofocus>--}}

                        {{--@if ($errors->has('code'))--}}
                            {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('code') }}</strong>--}}
                                    {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<div class="col-md-6 col-md-offset-4">--}}
                        {{--<button type="submit" class="btn btn-primary">--}}
                            {{--Verify Account--}}
                        {{--</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</form>--}}

        {{--@else--}}

            <form class="form-edit-add" role="form"
                  action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                  method="POST" enctype="multipart/form-data" autocomplete="off">
                <!-- PUT Method if we are editing -->
                @if(isset($dataTypeContent->id))
                    {{ method_field("PUT") }}
                @endif
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-bordered">
                            {{-- <div class="panel"> --}}
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="name">{{ __('voyager::generic.name') }}</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('voyager::generic.name') }}"
                                           value="{{ old('name', $dataTypeContent->name ?? '') }}">
                                </div>

                                <div class="form-group">
                                    <label for="position">{{ __('generic.position') }}</label>
                                    <input type="text" class="form-control" id="position" name="position" placeholder="{{ __('generic.position') }}"
                                           value="{{ old('position', $dataTypeContent->position ?? '') }}">
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6 mb-3">
                                        <label for="phone">{{ __('voyager::generic.phone') }}</label>
                                        <input type="tel" class="form-control" id="phone" name="phone" placeholder="+77XXXXXXX"
                                               value="{{ old('phone', $dataTypeContent->phone ?? '') }}">
                                        <span class="validation-input-msg"></span>
                                    </div>
                                    <div class="form-group col-md-6 mb-3">
                                        <label for="phone">{{ __('generic.verification') }}</label><br>
                                        <a href="javascript:;" title="{{ __('generic.send') }} SMS" class="btn btn-success m-0" id="btnVerifyPhoneNumberModal">
                                            <i class="voyager-telephone"></i> <span class="hidden-xs hidden-sm">{{ __('generic.verify_via_sms') }}</span>
                                        </a>
                                    </div>


                                    {{--<div class="form-group col-md-6 mb-3">--}}
                                    {{--<label for="code">{{ __('generic.confirmation_code') }}</label>--}}
                                    {{--<input type="number" class="form-control" id="code" name="code" placeholder="{{ __('generic.confirmation_code') }}"--}}
                                    {{--value="{{ old('code', $dataTypeContent->code ?? '') }}">--}}
                                    {{--<span class="validation-input-msg"></span>--}}
                                    {{--<a id="btnVerifySms" class="btn btn-primary pull-right save m-0 position-absolute" style="right:15px;top: 26px;">--}}
                                    {{--{{__('generic.check')}}--}}
                                    {{--</a>--}}
                                    {{--</div>--}}
                                </div>

                                <div class="form-group">
                                    <label for="email">{{ __('voyager::generic.email') }}</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="{{ __('voyager::generic.email') }}"
                                           value="{{ old('email', $dataTypeContent->email ?? '') }}">
                                </div>

                                <div class="form-group">
                                    <label for="password">{{ __('voyager::generic.password') }}</label>
                                    @if(isset($dataTypeContent->password))
                                        <br>
                                        <small>{{ __('voyager::profile.password_hint') }}</small>
                                    @endif
                                    <input type="password" class="form-control" id="password" name="password"
                                           value="{{isset($dataTypeContent->id) ? '' : Illuminate\Support\Str::random(8)}}"
                                           autocomplete="new-password">
                                </div>

                                @can('editRoles', $dataTypeContent)
                                    <div class="form-group">
                                        <label for="default_role">{{ __('voyager::profile.role_default') }}</label>
                                        @php
                                            $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};

                                            $row     = $dataTypeRows->where('field', 'user_belongsto_role_relationship')->first();
                                            $options = $row->details;
                                        @endphp
                                        @include('voyager::formfields.relationship')
                                    </div>
                                    <div class="form-group">
                                        <label for="additional_roles">{{ __('voyager::profile.roles_additional') }}</label>
                                        @php
                                            $row     = $dataTypeRows->where('field', 'user_belongstomany_role_relationship')->first();
                                            $options = $row->details;
                                        @endphp
                                        @include('voyager::formfields.relationship')
                                    </div>
                                @endcan
                                @php
                                    if (isset($dataTypeContent->locale)) {
                                        $selected_locale = $dataTypeContent->locale;
                                    } else {
                                        $selected_locale = config('app.locale', 'en');
                                    }
                                @endphp
                                <div class="form-group">
                                    <label for="locale">{{ __('voyager::generic.locale') }}</label>
                                    <select class="form-control select2" id="locale" name="locale">
                                        @foreach (Voyager::getLocales() as $locale)
                                            <option value="{{ $locale }}"
                                                    {{ ($locale == $selected_locale ? 'selected' : '') }}>{{ $locale }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel panel-bordered panel-warning">
                            <div class="panel-body">
                                <div class="form-group">
                                    @if(isset($dataTypeContent->avatar))
                                        <img src="{{ filter_var($dataTypeContent->avatar, FILTER_VALIDATE_URL) ? $dataTypeContent->avatar : Voyager::image( $dataTypeContent->avatar ) }}" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
                                    @endif
                                    <input type="file" data-name="avatar" name="avatar">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary pull-right save">
                    {{ __('voyager::generic.save') }}
                </button>
            </form>

            <iframe id="form_target" name="form_target" style="display:none"></iframe>
            <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
                {{ csrf_field() }}
                <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
                <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
            </form>

            {{--@if ( $dataTypeContent->getKey() )--}}

                {{--@inject('user_model', 'App\User')--}}

                {{--@php--}}
                    {{--$user = $user_model->find($dataTypeContent->getKey());--}}
                    {{--$car_user = new \App\CarUser();--}}
                {{--@endphp--}}

                {{--<h2 class="page-title"><i class="icon voyager-dashboard"></i>{{__('profile.your_cars')}}</h2>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<div class="panel panel-bordered">--}}
                            {{--<div class="panel-body">--}}

                                {{--@include('admin.car_user.table')--}}

                                {{--<h4 class="title-form-h4">{{__('voyager::generic.add_new')}}</h4>--}}

                                {{--<form class="form-edit-add" role="form"--}}
                                      {{--action="{{ route('admin.car_user.store') }}"--}}
                                      {{--method="POST" enctype="multipart/form-data" autocomplete="off">--}}
                                    {{--{{ csrf_field() }}--}}

                                    {{--@include('admin.car_user.form-edit-add')--}}

                                    {{--<input type="hidden" name="user_id" value="{{$user->id}}">--}}

                                    {{--<button type="submit" class="btn btn-primary pull-right save">--}}
                                        {{--{{ __('voyager::generic.save') }}--}}
                                    {{--</button>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--@endif--}}


    </div>
@stop

@include('partials.modal-verify-phone', ['phone_input_id' => 'phone'])

@section('javascript')
    <script>
        $(document).ready(function () {

            function validatePhone(strNumber) {
                var phoneno = {{config('services.nexmo.phone_number_pattern')}};
                if((strNumber.match(phoneno))) {
                    return true;
                } else {
                    return false;
                }
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var phoneField = $("input[name=phone]");
            var codeField = $("input[name=code]");

            var validationPhoneField = phoneField.next('.validation-input-msg');
            var validationCodeField = codeField.next('.validation-input-msg');

            $("input#phone").on('keyup', function (e) {
                validationPhoneField.text('').removeClass('success error');
                if (validatePhone($(this).val())) {
                    validationPhoneField.text('Верный формат').addClass('success');
                } else {
                    validationPhoneField.text('Не валидный номмер, попробуйте формат +77XXXXXXX').addClass('error');
                }
            });

            $("#btnSendSms").click(function(e){
                e.preventDefault();
                var phoneNumber = phoneField.val();

                validationPhoneField.text('').removeClass('success error');

                if (validatePhone(phoneNumber)) {
                    validationPhoneField.text('Верный формат').addClass('success');
                } else {
                    validationPhoneField.text('Не валидный номмер, попробуйте формат +77XXXXXXX').addClass('error');
                }

                $.ajax({
                    type:'POST',
                    url: '/admin/verify/send/' + phoneNumber,
                    data: {phone: phoneNumber},
                    success: function(data) {
                        if (data.success) {
                            console.log(data.success);
                            validationPhoneField.text('<i class="icon voyager-check"></i> ' + data.success).addClass('success');
                        }
                        if (data.error) {
                            console.log(data.error);
                            validationPhoneField.text(data.error).addClass('error');
                        }
                    }
                });
            });

            $("#btnVerifySms").click(function(e){
                e.preventDefault();

                var phoneNumber = phoneField.val();
                var codeNumber = codeField.val();

                validationCodeField.text('').removeClass('success error');
                $.ajax({
                    type:'POST',
                    url: '/admin/verify/check/' + codeNumber,
                    data: {code: codeNumber, phone: phoneNumber},
                    success: function(data) {
                        if (data.success) {
                            console.log(data.success);
                            validationCodeField.text('Номер верифицирован успешно').addClass('success');
                        }
                        if (data.error) {
                            console.log(data.error);
                            validationCodeField.text('Неверный код').addClass('error');
                        }
                    },

                });
            });

        });

    </script>
@stop
