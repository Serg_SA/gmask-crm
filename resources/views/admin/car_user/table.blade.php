@php
    $exist_cars = $user->cars;
@endphp

@if (count($exist_cars) > 0)
    <h4 class="title-form-h4">{{__('profile.your_cars')}}</h4>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">{{__('profile.brand')}}</th>
            <th scope="col">{{__('profile.model')}}</th>
            <th scope="col">{{__('profile.license_plate')}}</th>
            <th scope="col" class="text-right">{{__('voyager::generic.actions')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($exist_cars as $k => $exist_car)
            <tr>
                <th scope="row">{{$k+1}}</th>
                <td>{{$exist_car->brand}}</td>
                <td>{{$exist_car->model}}</td>
                <td>{{$exist_car->pivot->number}}</td>
                <td>
                    <span class="pull-right">
                        @include('partials.button-modal-delete', ['model' => $exist_car->pivot, 'route_name' => 'admin.car_user.delete'])
                    </span>
                    <a href="{{ route('admin.car_user.edit', $exist_car->pivot->id) }}"
                       class="btn btn-sm btn-primary pull-right desctable" style="display:inline;">
                        <i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}
                    </a>
                    <a href="{{ route('admin.car_user.show', $exist_car->pivot->id) }}"
                       class="btn btn-sm btn-warning pull-right desctable" style="display:inline; margin-right:5px;">
                        <i class="voyager-eye"></i> {{ __('voyager::generic.view') }}
                    </a>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif