@extends('voyager::master')

@section('page_title', __('voyager::generic.edit').' '.__('profile.car'))

@php $user = $car_user->user @endphp

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-dashboard"></i> {{ __('voyager::generic.edit') }} {{ __('profile.car') }} &nbsp;
        <span class="pull-right">
        @can('delete', $car_user)
            @include('partials.button-modal-delete', ['route_name' => 'admin.car_user.delete', 'model' => $car_user] )
        @endcan
        </span>
        <a href="/admin/users/{{$user->id}}/edit" class="btn btn-sm btn-primary">
            <i class="voyager-person"></i> {{ __('profile.back_to_profile') }}
        </a>
    </h1>


@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered" style="padding-bottom:5px;">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="panel-body">

                        <h4 class="title-form-h4">{{$car_user->car->brand}} {{$car_user->car->model}} ({{__('profile.license_plate')}}: {{$car_user->number}})</h4>

                        <form class="form-edit-add" role="form"
                              action="{{ route('admin.car_user.update', $car_user->id) }}"
                              method="POST" enctype="multipart/form-data" autocomplete="off">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}

                            @include('admin.car_user.form-edit-add')

                            <button type="submit" class="btn btn-primary pull-right save">
                                {{ __('voyager::generic.save') }}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

