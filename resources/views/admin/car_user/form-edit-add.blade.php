<div class="form-group">
    <label for="model">{{ __('profile.model') }}</label>
    <select class="form-control select2-ajax-car"
            name="model"
            data-get-items-route="{{route('admin.cars.list')}}" {{isset($car_user->id) ? 'disabled' : ''}}>
        <option value="">{{__('voyager::generic.none')}}</option>
        @inject('car', 'App\Car')
        @php $cars = $car->all() @endphp
        @foreach($cars as $car)
            <option value="{{ $car->id }}" {{(isset($car_user->id) && $car->id === $car_user->car->id) ? 'selected' : '' }}>
                {{ $car->brand }}  {{ $car->model }}
            </option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="number">{{ __('profile.license_plate') }}</label>
    <input type="text" class="form-control" id="number" name="number" placeholder="{{ __('profile.license_plate') }}"
           value="{{ old('number', request('number') ?? ($car_user->number ?? '')) }}">
</div>
<div class="form-group">
    <label for="number">{{ __('profile.photos') }}</label>
    <div id="cars-dropzone">
        <dropzone-component
                :text-placeholder="'{{__('profile.dropzone_placeholder')}}'"
                :text-delete="'{{__('voyager::generic.delete')}}'">
        </dropzone-component>
    </div>
</div>
<div class="form-group">
    <div class="row exist-photos-list">
        @if(count($car_user->photos) > 0)
            @foreach($car_user->photos as $photo)
                <div class="col-md-3">
                    <div class="exist-photos-list__item">
                        <a href="#" class="exist-photos-list__item--delete" id="{{$photo->id}}" title="{{__('voyager::generic.delete')}}"><i class="icon voyager-trash"></i></a>
                        <a href="{{asset('storage/'.$photo->path)}}" class="exist-photos-list__item--view" target="_blank" id="{{$photo->id}}"><i class="icon voyager-eye"></i> {{__('voyager::generic.view')}}</a>
                        <img src="{{asset('storage/'.$photo->path)}}" alt="">
                        <div class="exist-photos-list__item--alert"></div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>

@section('javascript')
    <script>
        new Vue({
            el: '#cars-dropzone',
        });

        $('document').ready(function () {

            $('.exist-photos-list').on('click', '.exist-photos-list__item--delete', function(e){
                e.preventDefault();
                var this_this = $(this);
                var id = $(this).attr('id');
                console.log(id);
                $.ajax({
                    type: 'GET',
                    url: '/admin/photos/delete/'+id,
                    data: {
                        '_method': 'DELETE',
                        '_token': '{{csrf_token()}}',
                    },
                    beforeSend: function (result) {
                    //
                    },
                    success: function(result) {
                        $(this_this).parent()
                            .addClass('deleted')
                            .find('.exist-photos-list__item--alert')
                            .text('{{__("generic.deleted")}}');
                    }
                });
            });

            $('.toggleswitch').bootstrapToggle();

            $('select.select2-ajax-car').each(function() {
                $(this).select2({
                    width: '100%',
                    ajax: {
                        url: $(this).data('get-items-route'),
                        data: function (params) {
                            var query = {
                                search: params.term,
                            }
                            return query;
                        }
                    }
                });

                $(this).on('select2:select',function(e){
                    var data = e.params.data;
                    if (data.id == '') {
                        // "None" was selected. Clear all selected options
                        $(this).val([]).trigger('change');
                    } else {
                        $(e.currentTarget).find("option[value='" + data.id + "']").attr('selected','selected');
                    }
                });

                $(this).on('select2:unselect',function(e){
                    var data = e.params.data;
                    $(e.currentTarget).find("option[value='" + data.id + "']").attr('selected',false);
                });
            });
        });
    </script>
@stop