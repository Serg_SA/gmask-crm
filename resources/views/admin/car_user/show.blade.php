@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.__('profile.car'))

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-dashboard"></i> {{ __('voyager::generic.viewing') }} {{ __('profile.car') }} &nbsp;

        @can('edit', $car_user)
            <a href="{{ route('admin.car_user.edit', $car_user->id) }}" class="btn btn-primary">
                <span class="glyphicon glyphicon-pencil"></span> {{ __('voyager::generic.edit') }}
            </a>
        @endcan
        <span class="pull-right">
            @can('delete', $car_user)
                @include('partials.button-modal-delete', ['route_name' => 'admin.car_user.delete', 'model' => $car_user] )
            @endcan
        </span>
    </h1>
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered" style="padding-bottom:5px;">
                    <div class="panel-body">
                        <form action="">
                            <div class="form-group">
                                <label for="model">{{ __('profile.model') }}</label>
                                <select class="form-control select2-ajax-car"
                                        name="model"
                                        data-get-items-route="{{route('admin.cars.list')}}" disabled>
                                    <option selected>
                                        {{ $car_user->car->brand }}  {{ $car_user->car->model }}
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="number">{{ __('profile.license_plate') }}</label>
                                <input type="text" class="form-control" id="number" name="number" placeholder="{{ __('profile.license_plate') }}"
                                       value="{{ $car_user->number }}" disabled>
                            </div>
                            <div class="form-group">
                                <div class="row exist-photos-list">
                                    @if(count($car_user->photos) > 0)
                                        @foreach($car_user->photos as $photo)
                                            <div class="col-md-3">
                                                <div class="exist-photos-list__item">
                                                    <a href="{{asset('storage/'.$photo->path)}}" class="exist-photos-list__item--view" target="_blank" id="{{$photo->id}}"><i class="icon voyager-eye"></i> {{__('voyager::generic.view')}}</a>
                                                    <img src="{{asset('storage/'.$photo->path)}}" alt="">
                                                    <div class="exist-photos-list__item--alert"></div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop