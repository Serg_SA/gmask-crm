@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing'))

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-pie-graph"></i> Отчет
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content report container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @include('voyager::alerts')
                        <div class="calendar">
                            <form action="" id="dateForm">
                                <div>
                                    <h6>Период: </h6>
                                    <input type="text" name="date" id="date" class="pulse" placeholder='&#128197; Завершение заказа "с" - "по"'>
                                </div>
                                <div class="calendar__statuses">
                                    <h6>Статус: </h6>
                                    <div>
                                        <label for="in_process">
                                            <input type="checkbox" name="status[]" id="in_process" value="in_process">
                                            <span>В процессе</span>
                                        </label>
                                        <label for="completed">
                                            <input type="checkbox" name="status[]" id="completed" value="completed">
                                            <span>Готово</span>
                                        </label>
                                        <label for="completed_not_happy">
                                            <input type="checkbox" name="status[]" id="completed_not_happy" value="completed_not_happy">
                                            <span>Готово (не доволен)</span>
                                        </label>
                                        <label for="released">
                                            <input type="checkbox" name="status[]" id="released" value="released" checked>
                                            <span>Выдан</span>
                                        </label>
                                        <label for="released_not_happy">
                                            <input type="checkbox" name="status[]" id="released_not_happy" value="released_not_happy">
                                            <span>Выдан (не доволен)</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="calendar__actions">
                                    <h6>Действия: </h6>
                                    <button type="submit" disabled>Применить</button>
                                </div>
                            </form>
                        </div>
                        <div class="report-results table-responsive">
                            <table id="ordersSearchResults" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Заказ</th>
                                        <th>Автомобиль</th>
                                        <th>Клиент</th>
                                        <th>Мастер</th>
                                        <th>Начало</th>
                                        <th>Завершение</th>
                                        <th><span class="completed">Completed</span></th>
                                        <th><span class="released">Released</span></th>
                                        <th>Сумма</th>
                                        <th>Статус</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="11">
                                            <span class="report-alert"></span>
                                            <span id="ordersSearchTotal"></span>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('javascript')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ru.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/themes/dark.css">
@stop

@push('javascript')
    <script>

      (function() {

        const loader = document.getElementById('voyager-loader');
        let table = document.getElementById('ordersSearchResults');
        let tableBody = table ? table.getElementsByTagName('tbody')[0] : null;
        let totalSection = document.getElementById('ordersSearchTotal');
        const form = document.getElementById('dateForm');

        if (!tableBody || !totalSection || !form) return;

        function showResults(data) {
          data.orders.forEach(order => {
            let row = `<tr>
                <td>${order.id}</td>
                <td>
                    <a href="${order.edit_link}" title="Редактировать заказ [${order.name}]"><span class="report-results__name">${order.name} </span> <i class="voyager-edit"></i></a> <br>
                    ${order.items.map((item, key) => `${key+1}) ${item.service.name} (<em>Цена: ${item.service.price} ₸</em>) <b>Кастомная цена: ${item.price} ₸</b>`).join('<br>')}
                </td>
                <td>${order.car}</td>
                <td>${order.user.name}</td>
                <td>${order?.master?.name}</td>
                <td><time>${order.datetime_start_formated}</time></td>
                <td class="${order.status === 'in_process' ? 'in_process' : ''}">
                    <time>${order.datetime_end_formated}</time>
                    ${order.in_process_diff ? order.in_process_diff : ''}
                </td>
                <td class="${order.status.includes('completed') ? order.status : ''}">
                    <time>${order.completed_datetime_end_formated}</time>
                    ${order.completed_diff ? order.completed_diff : ''}
                </td>
                <td class="${order.status.includes('released') ? order.status : ''}"><time>${order.released_datetime_end_formated}</time></td>
                <td><b>${order.total_price}</b></td>
                <td><span class="order_status_label" data-status-label="${order.status_label}">${order.status_label}</span></td>
                </tr>`;
            tableBody.innerHTML += row;
          });
          totalSection.innerHTML += data.orders_total;
        }

        function handleSubmit(event) {
          console.log(event)
          event.preventDefault();

          const formData = new FormData(event.target);
          const dateStr = formData.get('date');
          const dateField = event.target.querySelector('#date');
          const formButton = event.target.querySelector('button[type="submit"]');
          const alert = document.querySelector('.report-alert');
          if ( !dateField || !formButton || !alert )
            return;

          const statuses = [];
          formData.forEach((value, key) => {
            if (key === 'status[]') {
              statuses.push(value);
            }
          });

          if (!dateStr.includes(' — ')) {
            dateField.style.borderColor = 'blue';
            setTimeout(() => {
              dateField.style.borderColor = '';
            }, 1000)
          }
          formButton.disabled = false;

          $.ajax({
            type: 'GET',
            url: '{{ route('admin.report.search') }}',
            data: { date_between: dateStr, status: statuses },
            beforeSend: function( xhr ) {
              loader.style.display = 'block';
              tableBody.innerHTML = '';
              totalSection.innerHTML = '';
            },
            success: function(data) {
              console.log(data);
              if (data.error) {
                alert.innerHTML = data.error;
                alert.classList.add('alert', 'alert-danger');
              }
              if (data.orders) {
                if (data.orders.length > 0) {
                  showResults(data);
                  alert.innerHTML = ''
                  alert.classList.remove('alert', 'alert-danger');
                } else {
                  alert.innerHTML = 'По данному запросу ничего не найдено'
                  alert.classList.add('alert', 'alert-danger');
                }
              } else {
                alert.innerHTML = 'Что-то пошло нет так :(';
                alert.classList.add('alert', 'alert-danger');
              }

              loader.style.display = 'none';
            }
          });
        }

        flatpickr("#date", {
          dateFormat: "Y-m-d",
          mode: "range",
          locale: "ru",
          onChange: function(selectedDates, dateStr, instance) {
            const formButton = document.querySelector('.calendar button[type="submit"]');
            if (selectedDates.length === 2) {

              instance.element.classList.add('selected');
              instance.element.classList.remove('pulse');
              formButton.disabled = false;
            } else {
              instance.element.classList.remove('selected');
              instance.element.classList.add('pulse');
              formButton.disabled = true;
            }
          }
        });
        form.addEventListener('submit', handleSubmit);
      })();

    </script>
@endpush