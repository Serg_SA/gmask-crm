@if (count($cards) > 0)
    <div class="agent-order-cards">
        @foreach($cards as $k => $card)
            <span class="btn-show-qr-code" data-number="{{$card->number}}">
                {{$card->number}} <span>{{$card->meters ?? '?'}} <em>п/м</em></span>
            </span>
        @endforeach
    </div>
@endif
@include('agents/partials/modal-warranty-qr-code')
@push('javascript')
    <script>
        (function() {
            const getDataAgentWarrantyCard = function (el) {
                const number = el.dataset.number;
                const row = el.closest('.agent-cards-list__body-row');
                const rows = document.querySelectorAll('.agent-cards-list__body-row')
                const toggle = row.querySelector('.agent-cards-list__body-row-toggle')
                const load = row.querySelector('.load');
                const for_fields = row.querySelector('.agent-card-fields');
                if (!load) return;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('agent.cards.get_card_ajax') }}',
                    data: { number: number },
                    beforeSend: function( xhr ) {
                        row.classList.add('loading');
                        rows.forEach(function (el) {
                            el.style.pointerEvents = 'none';
                        });
                    },
                    success: function(data) {
                        toggle.style.height = toggle.children[0].offsetHeight + 'px';
                        row.classList.remove('loading');
                        rows.forEach(function (el) {
                            el.style.pointerEvents = '';
                        });
                        if ( data.success ) {
                            let html = '';
                            for (const key in data.fields) {
                                html += `<li><span>${key}:</span><span></span><span>${data.fields[key]}</span></li>`;
                            }
                            console.log(load)
                            load.remove();
                            for_fields.innerHTML = html;
                            toggle.style.height = toggle.children[0].offsetHeight + 'px';
                        }
                        if ( data.error ) {
                            console.log(data.error);
                        }
                    }
                });
            }
        })();
    </script>
@endpush