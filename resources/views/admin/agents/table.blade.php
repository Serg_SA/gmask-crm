@php
    $exist_orders = (App\Agent::with('orders')->find($agent_id))->orders;
@endphp

@if (count($exist_orders) > 0)
    <h4 class="title-form-h4">{{__('generic.agent_orders')}}</h4>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">{{__('generic.number')}}</th>
            <th scope="col">{{__('generic.warranty_template')}}</th>
            <th scope="col">{{__('generic.linear_meters_quantity')}}</th>
            <th scope="col">{{__('generic.linear_meters_left_quantity')}}</th>
            <th scope="col" class="text-right">{{__('voyager::generic.actions')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($exist_orders as $k => $exist_order)
            <tr>
                <th scope="row">{{$k+1}}</th>
                <td>{{$exist_order->number}}</td>
                <td>
                    {{$exist_order->warranty_template}}
                    @include('admin/agents/agent-cards', ['cards' => $exist_order->cards])
                </td>
                <td>{{$exist_order->meters}}</td>
                <td>{{$exist_order->meters_left}}</td>
                <td>
                    <span class="pull-right">
                        @include('partials.button-modal-delete', ['model' => $exist_order, 'route_name' => 'admin.agent_orders.delete'])
                    </span>
                    {{--<a href="{{ route('admin.car_user.edit', $exist_order->pivot->id) }}"--}}
                    {{--class="btn btn-sm btn-primary pull-right desctable" style="display:inline;">--}}
                    {{--<i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}--}}
                    {{--</a>--}}
                    {{--<a href="{{ route('admin.car_user.show', $exist_order->pivot->id) }}"--}}
                    {{--class="btn btn-sm btn-warning pull-right desctable" style="display:inline; margin-right:5px;">--}}
                    {{--<i class="voyager-eye"></i> {{ __('voyager::generic.view') }}--}}
                    {{--</a>--}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
