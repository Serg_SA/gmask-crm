<h4 class="title-form-h4">{{__('generic.add_order')}}</h4>
<form role="form"
      class="row form-edit-add"
      action="{{route('admin.agent_orders.store')}}"
      method="POST">

    {{ csrf_field() }}

    @if (count($errors) > 0)
        <div class="col">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    <div class="form-group col-md-7">
        <label class="control-label" for="warranty_template">{{ __('generic.warranty_template') }}</label>
        <select class="form-control" name="warranty_template">
            <option value="">{{__('voyager::generic.none')}}</option>
            @inject('template_model', 'App\AgentCardTemplate')
            @php $templates = $template_model->all() @endphp
            @foreach($templates as $template)
                <option value="{{ $template->name }}" {{old('template') === $template->name ? 'selected' : ''}}>
                    {{ $template->name }}
                </option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-3">
        <label class="control-label" for="number">{{ __('generic.number') }}</label>
        <input type="text" name="number" value="{{old('number')}}" class="form-control">
    </div>
    <div class="form-group col-md-2">
        <label class="control-label" for="meters">{{ __('generic.linear_meters_quantity') }}</label>
        <input type="number" name="meters" value="{{old('meters') ?? 1}}" min="1" max="1000" class="form-control">
    </div>

    <input type="hidden" name="agent_id" value="{{$agent_id}}">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.add') }}</button>
    </div>
</form>
