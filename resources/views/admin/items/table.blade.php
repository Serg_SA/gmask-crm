@php
    $exist_items = (App\Order::with('items')->find($order_id))->items;
@endphp

@if (count($exist_items) > 0)
    <h4 class="title-form-h4">{{__('generic.order_services')}}</h4>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">{{__('voyager::generic.name')}}</th>
            <th scope="col">{{__('generic.notice')}}</th>
            <th scope="col">{{__('generic.quantity')}}</th>
            <th scope="col">{{__('generic.total_price')}}</th>
            <th scope="col" class="text-right">{{__('voyager::generic.actions')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($exist_items as $k => $exist_item)
            <tr>
                <th scope="row">{{$k+1}}</th>
                <td>
                    {{$exist_item->service->name}} ({{$exist_item->service->price}} ₸) <br>
                    {!! $exist_item->price ? '<small class="text-info">'.__('generic.custom_price'). ': '.$exist_item->price.' ₸</small>' : ''!!}
                </td>
                <td>{{$exist_item->notice}}</td>
                <td>{{$exist_item->qty}}</td>
                <td>{{$exist_item->qty * ($exist_item->price ?? $exist_item->service->price)}} ₸</td>
                <td>
                    <span class="pull-right">
                        @include('partials.button-modal-delete', ['model' => $exist_item, 'route_name' => 'admin.items.delete'])
                    </span>
                    <span class="pull-right">
                        @include('partials.button-modal-edit-item', ['model' => $exist_item])
                    </span>
                    {{--<a href="{{ route('admin.car_user.edit', $exist_item->pivot->id) }}"--}}
                       {{--class="btn btn-sm btn-primary pull-right desctable" style="display:inline;">--}}
                        {{--<i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}--}}
                    {{--</a>--}}
                    {{--<a href="{{ route('admin.car_user.show', $exist_item->pivot->id) }}"--}}
                       {{--class="btn btn-sm btn-warning pull-right desctable" style="display:inline; margin-right:5px;">--}}
                        {{--<i class="voyager-eye"></i> {{ __('voyager::generic.view') }}--}}
                    {{--</a>--}}
                </td>
            </tr>
        @endforeach
        <tr>
            <td colspan="6" class="text-right">
                <strong>{{__('generic.total')}}: </strong>
                <span class="font-weight-bold">{{App\Facades\Order::getTotalPrice($order_id)}}</span>
            </td>
        </tr>
        </tbody>
    </table>
@endif