<h4 class="title-form-h4">{{__('generic.add_service_to_this_order')}}</h4>
<form role="form"
      class="form-edit-add"
      action="{{route('admin.items.store')}}"
      method="POST">

    {{ csrf_field() }}

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="form-group col-md-7">
        <label class="control-label" for="service_id">{{ __('generic.service') }}</label>
        <select class="form-control select2-ajax-item"
                name="service_id"
                data-get-items-route="{{route('admin.services.list')}}">
            <option value="">{{__('voyager::generic.none')}}</option>
            @inject('service_model', 'App\Service')
            @php $services = $service_model->all() @endphp
            @foreach($services as $service)
                <option value="{{ $service->id }}" {{old('service_id') === $service->id ? 'selected' : ''}}>
                    {{ $service->name }}  ({{ $service->price }} ₸)
                </option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-3">
        <label class="control-label" for="price">{{ __('generic.price') }} (₸)</label>
        <input type="number" name="price" value="{{old('price')}}" min="0" max="10000000" class="form-control">
    </div>
    <div class="form-group col-md-2">
        <label class="control-label" for="qty">{{ __('generic.quantity') }}</label>
        <input type="number" name="qty" value="{{old('qty') ?? 1}}" min="1" max="1000" class="form-control">
    </div>

    <div class="form-group col-md-12">
        <label class="control-label" for="notice">{{ __('generic.notice') }}</label>
        <textarea name="notice" class="form-control">{{old('notice')}}</textarea>
    </div>
    <input type="hidden" name="order_id" value="{{$order_id}}">
    <div class="panel-footer">
        <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.add') }}</button>
    </div>
</form>

@section('javascript')
    <script>
        $(document).ready(function () {
            $('select.select2-ajax-item').each(function() {
                $(this).select2({
                    width: '100%',
                    ajax: {
                        url: $(this).data('get-items-route'),
                        data: function (params) {
                            var query = {
                                search: params.term,
                            };
                            return query;
                        }
                    }
                });

                $(this).on('select2:select',function(e){
                    var data = e.params.data;
                    if (data.id == '') {
                        // "None" was selected. Clear all selected options
                        $(this).val([]).trigger('change');
                    } else {
                        $(e.currentTarget).find("option[value='" + data.id + "']").attr('selected','selected');
                        $('input[name=price]').val(data.price);
                    }
                });

                $(this).on('select2:unselect',function(e){
                    var data = e.params.data;
                    $(e.currentTarget).find("option[value='" + data.id + "']").attr('selected',false);
                });
            });
        });
    </script>
@stop