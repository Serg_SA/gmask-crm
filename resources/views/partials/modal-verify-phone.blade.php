<div class="modal modal-success fade" tabindex="-1" id="verifyPhoneNumberModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="voyager-telephone"></i>
                    {{ __('generic.phone_number_verification') }}
                </h4>
            </div>
            <div class="modal-body">
                @include('partials.form-verify-phone')
            </div>
        </div>
    </div>
</div>

@push('javascript')
    <script>
        $(document).ready(function () {
            $('#btnVerifyPhoneNumberModal').on('click', function (e) {
                $('#verifyPhoneNumberModal').modal('show');
            });
        });
    </script>
@endpush
