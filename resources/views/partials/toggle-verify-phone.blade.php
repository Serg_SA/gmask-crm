<h5><a href="javascript:void(0);" id="verifyToggle">{{ __('generic.phone_number_verification') }}</a></h5>
<div id="verifyToggleSection">
    <div class="form-group" id="formGroupPhone">
        <div class="row">
            <div class="col-md-12 mb-0"><label for="verify_phone">{{ __('voyager::generic.phone') }}</label></div>
            <div class="col-md-8 mb-1">
                <input type="tel" class="form-control" id="verify_phone" name="verify_phone" placeholder="+77XXXXXXX"
                       value="{{ old('verify_phone') }}">
            </div>
            <div class="col-md-4 mb-1">
                <a id="btnSendSms" class="btn btn-primary m-0">
                    {{__('voyager::generic.submit')}} SMS
                </a>
            </div>
            <div class="col-md-12 mb-0">
                <span id="phoneValidationMsg" class="validation-input-msg"></span>
            </div>
        </div>
    </div>


    <div class="form-group" id="formGroupCode">
        <div class="row">
            <div class="col-md-12 mb-0"><label for="verify_code">{{ __('generic.confirmation_code') }}</label></div>
            <div class="col-md-8 mb-1">
                <input type="number" class="form-control" id="verify_code" name="verify_code" placeholder="{{ __('generic.confirmation_code') }}"
                       value="{{ old('verify_code') }}">
            </div>
            <div class="col-md-4 mb-1">
                <a id="btnVerifySms" class="btn btn-primary save">
                    {{__('generic.check')}}
                </a>
            </div>
            <div class="col-md-12 mb-0">
                <span id="codeValidationMsg" class="validation-input-msg"></span>
            </div>
        </div>
    </div>
</div>

@push('javascript')
    <script>
        $(document).ready(function () {

            // Functions
            function validatePhone(strNumber) {
                var phone = {{config('services.nexmo.phone_number_pattern')}};
                if((strNumber.match(phone))) {
                    return true;
                } else {
                    return false;
                }
            }
            // End Functions

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var phoneInputSourceForm = $('#{{$phone_input_id}}');
            var phoneField = $("input[name=verify_phone]");
            var codeField = $("input[name=verify_code]");
            var validationPhoneField = $('#phoneValidationMsg');
            var validationCodeField = $('#codeValidationMsg');

            $('#verifyToggle').click(function (e) {
                $('#verifyToggleSection').find('#verify_phone').val(phoneInputSourceForm.val());
                $('#verify_phone').keyup();
            });

            $("input#verify_phone").on('keyup', function (e) {
                validationPhoneField.text('').removeClass('success error');
                if (validatePhone($(this).val())) {
                    validationPhoneField.text('Верный формат').addClass('success');
                } else {
                    validationPhoneField.text('Не валидный номмер, попробуйте формат +77XXXXXXX').addClass('error');
                }
            });

            $("#btnSendSms").click(function(e){
                e.preventDefault();
                var phoneNumber = phoneField.val();

                validationPhoneField.text('').removeClass('success error');

                if (validatePhone(phoneNumber)) {
                    validationPhoneField.text('Верный формат').addClass('success');

                    $.ajax({
                        type:'POST',
                        url: '/admin/verify/send/' + phoneNumber,
                        data: {phone: phoneNumber},
                        success: function(data) {
                            if (data.success) {
                                console.log(data.success);
                                $('#formGroupPhone').html('<div class="big-alert-success"> <i class="icon voyager-check"></i> ' + data.success + '</div>');
//                            validationPhoneField.text('<i class="icon voyager-check"></i> ' + data.success).addClass('success');
                            }
                            if (data.error) {
                                console.log(data.error);
                                validationPhoneField.text(data.error).addClass('error');
                            }
                        }
                    });

                } else {
                    validationPhoneField.text('Не валидный номмер,попробуйте формат +77XXXXXXX').addClass('error');
                }
            });

            $("#btnVerifySms").click(function(e){
                e.preventDefault();

                var phoneNumber = phoneField.val();
                var codeNumber = codeField.val();

                validationCodeField.text('').removeClass('success error');
                $.ajax({
                    type:'POST',
                    url: '/admin/verify/check/' + codeNumber,
                    data: {code: codeNumber, phone: phoneNumber},
                    success: function(data) {
                        if (data.success) {
                            console.log(data.success);
                            $('#formGroupPhone').hide();
                            $('#formGroupCode').html('<div class="big-alert-success"> <i class="icon voyager-check"></i> ' + data.success + '</div>');
                            phoneInputSourceForm.val(phoneNumber);
                            phoneInputSourceForm.keyup().next('.validation-input-msg').text('{{__('generic.phone_number_is_verified')}}');
                        }
                        if (data.error) {
                            console.log(data.error);
                            validationCodeField.text('Неверный код').addClass('error');
                        }
                    },

                });
            });

        });

    </script>
@endpush