<div class="modal modal-info fade" tabindex="-1" id="showWarrantyQRCode" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><img src="{{asset('storage')}}/qr.svg" alt="">
                    QR code ( <strong class="header-number"></strong> )
                </h4>
            </div>
            <div class="modal-body">

                <div class="form-edit-add" role="form" >
                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-body">
                            <div class="loader-primary"><div class="spiner-primary"></div></div>
                            <div class="modal-qr-code">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@push('javascript')
    <script>
        $(document).ready(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Define variables
            var modal = document.getElementById('showWarrantyQRCode');
            var headerNumber = modal.querySelector('.header-number');
            var qrCode = modal.querySelector('.modal-qr-code');
            var qrLoader = modal.querySelector('.loader-primary');

            // Event Listeners for buttons
            var buttonsModal = document.querySelectorAll('.btn-show-qr-code');
            if (buttonsModal.length > 0) {
                buttonsModal.forEach(function (el) {
                    el.addEventListener('click', function(e) {
                        e.preventDefault();
                        $(modal).modal('show');
                        let number = $(this).data('number');
                        headerNumber.innerText = '';
                        headerNumber.innerText = number;
                        console.log(number);
                        data = {
                            number: number,
                        };

                        $.ajax({
                            type:'POST',
                            url: '{{route('admin.cards.qrcode_ajax')}}',
                            data: data,
                            beforeSend: function( xhr ) {
                                qrLoader.classList.add('loading');
                            },
                            success: function(data) {
                                console.log(data);
                                qrLoader.classList.remove('loading');
                                if (data.success ?? data.link) {
                                    let imgQRCode = document.createElement('img');
                                    imgQRCode.setAttribute('src', data.link);
                                    qrCode.appendChild(imgQRCode);
                                    console.log(data.success);
                                }
                                if (data.error) {
                                    console.log(data.error);
                                    this_this.find('.alert').addClass('alert-danger').text(data.error).show();
                                    setTimeout(function () {
                                        this_this.find('.alert-danger').removeClass('alert-danger').hide().text('');
                                    }, 2000);
                                }
                            }
                        });
                    });
                })
            }

            $('#showWarrantyQRCode').on('hidden.bs.modal', function (e) {
                e.target.querySelector('.modal-qr-code').innerHTML = '';
            });
        });

    </script>
@endpush
