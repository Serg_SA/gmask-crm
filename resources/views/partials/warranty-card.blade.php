@inject('card_service', 'App\Services\CardService')
<span class="card">
    <strong><i class="voyager-certificate"></i> {{__('generic.warranty_card')}}:
        <span>
            {{$card->number}}
            <a href="#" data-number="{{$card->number}}" class="btn-show-qr-code">
                <img src="{{asset('storage')}}/qr.svg" alt="">
            </a>
            <a href="#" data-card_id="{{$card->id}}" data-sent_sms="{{$card_service->countSentSMS($card)}}" class="btn-send-sms">
                <img src="{{asset('storage')}}/sms.svg" alt="">
            </a>
        </span>
    </strong>
    <button class="button-toggle"><i class="voyager-angle-down"></i></button>

    @php
        $card_data = $card_service->getSingleOsmicardWarrantyFields($card->number)
    @endphp
    <ul>
        @foreach($card_data as $key => $value)
            @if ( '-empty-' == $value )
                @php $value = ''; @endphp
            @endif
            <li><strong>{{$key}}:</strong> {!! $value !!}</li>
        @endforeach
        <li><button class="btn-register-warranty-event" data-number="{{$card->number}}">Регистрация гарантийного события</button></li>
    </ul>
</span>
