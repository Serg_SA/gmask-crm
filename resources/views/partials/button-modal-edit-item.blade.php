<a href="javascript:;" title="{{ __('voyager::generic.edit') }}" class="btn btn-warning edit" data-id="{{ $model->id }}" id="edit-{{ $model->id }}" >
    <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.edit') }}</span>
</a>

<div class="modal modal-warning fade" tabindex="-1" id="edit_modal_{{$model->id}}" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="voyager-edit"></i>
                    {{ __('voyager::generic.edit') }}
                </h4>
            </div>
            <div class="modal-footer">
                @include('admin.items.form-edit', ['item' => $model])
            </div>
        </div>
    </div>
</div>

@push('javascript')
    <script>
        $(document).ready(function () {
            $('#edit-{{$model->id}}').on('click', function (e) {
                $('#edit_modal_{{$model->id}}').modal('show');
            });
        });
    </script>
@endpush
