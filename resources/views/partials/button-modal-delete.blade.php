<a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete" data-id="{{ $model->id }}" id="delete-{{ $model->id }}" >
    <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
</a>

<div class="modal modal-danger fade" tabindex="-1" id="delete_modal_{{$model->id}}" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="voyager-trash"></i>
                    {{ __('voyager::generic.delete_question') }}
                </h4>
            </div>
            <div class="modal-footer">
                <form action="{{ route($route_name, $model->id) }}" id="delete_form_{{$model->id}}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger pull-right delete-confirm"
                           value="{{ __('voyager::generic.delete_confirm') }}">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
            </div>
        </div>
    </div>
</div>

@push('javascript')
    <script>
        $(document).ready(function () {
            $('#delete-{{$model->id}}').on('click', function (e) {
                $('#delete_modal_{{$model->id}}').modal('show');
            });
        });
    </script>
@endpush
