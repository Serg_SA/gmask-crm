<div class="modal modal-success fade" tabindex="-1" id="addNewUserModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="voyager-user"></i>
                    {{ __('generic.add_new_user') }}
                </h4>
            </div>
            <div class="modal-body">

                <div class="form-edit-add" id="formAddNewUserAjax" role="form" >
                    {{ csrf_field() }}

                    <div class="row">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name">{{ __('voyager::generic.name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('voyager::generic.name') }}"
                                       value="{{ old('name') }}">
                            </div>

                            <div class="form-group">
                                <label for="email">{{ __('voyager::generic.email') }}</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="{{ __('voyager::generic.email') }}"
                                       value="{{ old('email') }}">
                            </div>

                            <div class="form-group">
                                <label for="phone">{{ __('voyager::generic.phone') }}</label>
                                <input type="tel" class="form-control" id="phone" name="phone" placeholder="+77XXXXXXX"
                                       value="{{ old('phone') }}">
                                <span class="validation-input-msg"></span>
                            </div>
                            <div class="form-group">
                                @include('partials.toggle-verify-phone', ['phone_input_id' => 'phone'])
                            </div>


                            <button class="btn btn-primary pull-right save" id="submitFormAddNewUserAjax">
                                {{ __('voyager::generic.add') }}
                            </button>
                        </div>

                        <div class="alert alert-success" style="display: none"></div>
                    </div>



                </div>
            </div>

        </div>
    </div>
</div>

@push('javascript')
    <script>
        $(document).ready(function () {

            // Functions
            function validatePhone(strNumber) {
                var phone = {{config('services.nexmo.phone_number_pattern')}};
                if((strNumber.match(phone))) {
                    return true;
                } else {
                    return false;
                }
            }
            // End Functions

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            {{--var phoneInputSourceForm = $('#{{$phone_input_id}}');--}}
            var rootDivForm = $('#formAddNewUserAjax');
            var nameField = rootDivForm.find('input[name=name]');
            var phoneField = rootDivForm.find('input[name=phone]');
            var emailField = rootDivForm.find('input[name=email]');

            var validationPhoneField = phoneField.next('.validation-input-msg');

            $('#btnAddNewUserModal').on('click', function (e) {
                $('#addNewUserModal').modal('show');
            });

            $('#verifyToggle').click(function () {
               $('#verifyToggleSection').toggleClass('active');
            });

            $("input#phone").on('keyup', function (e) {
                validationPhoneField.text('').removeClass('success error');
                if (validatePhone($(this).val())) {
                    validationPhoneField.text('Верный формат').addClass('success');
                } else {
                    validationPhoneField.text('Не валидный номмер, попробуйте формат +77XXXXXXX').addClass('error');
                }
            });

            $("#submitFormAddNewUserAjax").on('click', function(e){

                e.preventDefault();

                var nameUser = nameField.val();
                var emailUser = emailField.val();
                var phoneNumber = phoneField.val();

                validationPhoneField.text('').removeClass('success error');

                if (validatePhone(phoneNumber)) {
                    validationPhoneField.text('Верный формат').addClass('success');
                    data = {
                        name: nameUser, phone: phoneNumber
                    };
                    if (emailUser) {
                        data.email = emailUser;
                    }
                    $.ajax({
                        type:'POST',
                        url: '{{route('admin.users.store_ajax')}}',
                        data: data,
                        success: function(data) {
                            console.log(data);
                            if (data.success) {
                                console.log(data.success);
                                rootDivForm.find('.alert').addClass('alert-success').text(data.success ? 'Пользователь добавлен успешно':'').show();
                                setTimeout(function () {
                                    rootDivForm.find('.alert-success').removeClass('alert-success').hide().text('');
                                }, 3000);
                            }
                            if (data.error) {
                                console.log(data.error);
                                rootDivForm.find('.alert').addClass('alert-danger').text(data.error).show();
                                setTimeout(function () {
                                    rootDivForm.find('.alert-danger').removeClass('alert-danger').hide().text('');
                                }, 3000);
                            }
                        }
                    });

                } else {
                    validationPhoneField.text('Не валидный номмер, попробуйте формат +77XXXXXXX').addClass('error');
                }
            });

        });

    </script>
@endpush
