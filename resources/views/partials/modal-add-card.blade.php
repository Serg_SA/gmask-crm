<div class="modal modal-success fade" tabindex="-1" id="addNewCardModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="voyager-certificate"></i>
                    {{ __('generic.add_new_card') }}
                </h4>
            </div>
            <div class="modal-body">

                <div class="form-edit-add" role="form" >
                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-body">
                            <form action="" id="formAddNewCardAjax">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="full_name">{{ __('generic.full_name') }}</label>
                                    <input type="text" class="form-control" id="full_name" name="full_name" placeholder=""
                                           value="{{ old('full_name') }}" required>
                                </div>

                                @inject('user', App\User)
                                @php
                                    $masters = $user::master()->get();
                                @endphp
                                <div class="form-group">
                                    <label for="master">{{ __('generic.master') }}</label>
                                    <select name="master" class="form-control" required>
                                        <option value=""></option>
                                        @foreach($masters as $master)
                                        <option value="{{$master->name}}">{{$master->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="car_number">{{ __('generic.car_vin_and_number') }}</label>
                                    <input type="text" class="form-control" id="car_number" name="car_number" placeholder=""
                                           value="{{ old('car_number') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="roll_number">{{ __('generic.roll_number') }} / {{ __('generic.batch_number') }}</label>
                                    <input type="text" class="form-control" id="roll_number" name="roll_number" placeholder=""
                                           value="{{ old('roll_number') }}" required>
                                </div>

                                <input type="hidden" name="item_id" value="">

                                <div class="alert alert-success" style="display: none"></div>
                                <div class="alert-system"></div>
                                <button type="submit" class="btn btn-primary pull-right save" id="submitFormAddNewCardAjax">
                                    <span class="spiner"></span>
                                    {{ __('voyager::generic.add') }}
                                </button>
                            </form>
                        </div>


                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

@push('javascript')
    <script>
        $(document).ready(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Define variables
            var form = $('#formAddNewCardAjax');
            var fullNameField = form.find('input[name=full_name]');
            var masterField = form.find('select[name=master]');
            var carNumberField = form.find('input[name=car_number]');
            var rollNumberField = form.find('input[name=roll_number]');
            var itemIdField = form.find('input[name=item_id]');

            // Event Listeners for buttons
            var buttonsModal = document.querySelectorAll('.btn-add-new-card');
            if (buttonsModal.length > 0) {
                buttonsModal.forEach(function (el) {
                    el.addEventListener('click', function(e) {
                        e.preventDefault();
                        $('#addNewCardModal').modal('show');
                        itemIdField.val('');
                        itemIdField.val($(this).data('item'));
                    });
                })
            }

            // On submit form
            form.on('submit', function(e){
                e.preventDefault();

                var this_this = $(this);
                var sumbit_button = $(this).find('button[type="submit"]');

                var fullName = fullNameField.val();
                var master = masterField.val();
                var carNumber = carNumberField.val();
                var rollNumber = rollNumberField.val();
                var itemId = itemIdField.val();

                data = {
                    full_name: fullName,
                    master: master,
                    car_number: carNumber,
                    roll_number: rollNumber,
                    item_id: itemId,
                };

                $.ajax({
                    type:'POST',
                    url: '{{route('admin.cards.store_ajax')}}',
                    data: data,
                    beforeSend: function( xhr ) {
                        sumbit_button.addClass('loading');
                    },
                    success: function(data) {
                        sumbit_button.removeClass('loading');
                        sumbit_button.hide();
                        console.log(data);
                        if (data.success ?? data.card) {
                            console.log(data);
                            console.log(data.success);

                            this_this.find('.alert').addClass('alert-success').html((data.success && data.card) ? 'Карта <strong>' + data.card + '</strong> успешно создана.' : '').show();
                            this_this.find('.alert-system').html('<string>Подождите, идет перезагрузка страницы ...</string>').show();
                            setTimeout(function () {
                                document.location.reload();
                            }, 2000)

                        }
                        if (data.error) {
                            console.log(data.error);
                            this_this.find('.alert').addClass('alert-danger').text(data.error).show();
                            setTimeout(function () {
                                this_this.find('.alert-danger').removeClass('alert-danger').hide().text('');
                                $('#addNewCardModal').modal('hide');
                            }, 2000);
                        }
                    }
                });
            });

        });

    </script>
@endpush
