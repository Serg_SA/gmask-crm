<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarOrder extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'car_order';

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function car()
    {
        return $this->belongsTo('App\Car');
    }

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }
}
