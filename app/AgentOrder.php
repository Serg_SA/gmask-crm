<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentOrder extends Model
{
    protected $fillable = ['warranty_template', 'number', 'meters', 'meters_left'];

    /**
     * Return Agent.
     */
    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }

    /**
     * Return Cards.
     */
    public function cards()
    {
        return $this->hasMany('App\AgentCard');
    }
}
