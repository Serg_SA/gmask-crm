<?php

namespace App\Services;

use App\Order;
use Illuminate\Support\Facades\DB;

class OrderService {

    public function getTotal($order_id)
    {
        $order = Order::with('items')->find($order_id);
        $total_amount = 0;
        if ($order && count($order->items) > 0) {
            foreach ($order->items as $item) {
                $total_amount = $total_amount + $item->price * $item->qty;
            }
        }
        return $total_amount;
    }

    public function getTotalPrice($order_id)
    {
        $formatted_total = number_format($this->getTotal($order_id), 0, '.', ' ');
        return  $formatted_total . ' ₸';
    }

    public function getTotalManyOrders($orders)
    {
        $total = 0;
        if (count($orders) < 1)
            return $total;

        foreach ($orders as $order) {
            $total += $this->getTotal($order->id);
        }

        return $total;
    }

    public function getTotalPriceManyOrders($orders)
    {
        $formatted_total = number_format($this->getTotalManyOrders($orders), 0, '.', ' ');
        return  $formatted_total . ' ₸';
    }

    public function getFullcalendarData()
    {
        $orders = DB::table('orders')->select('name as title', 'datetime_start as start', 'datetime_end as end')->get();
        $orders->map(function ($item, $key) {
            $item->start = str_replace(' ', 'T', $item->start);
            $item->end = str_replace(' ', 'T', $item->end);
            $item->allDay = false;
            return $item;
        });

        return $orders->toJson();
    }

    public function isGuaranteed($order)
    {
        $status = true;

        $items = $order->items;
        foreach ($items as $item) {
            if (!$item->card && $item->service->cardTemplate) {
                $status = false;
                break;
            }
        }
        return $status;
    }
}
