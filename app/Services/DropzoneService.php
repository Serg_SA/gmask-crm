<?php

namespace App\Services;

class DropzoneService {

    public function generateName($file)
    {
        if (!is_file($file))
            return false;

        $extension = $file->getClientOriginalExtension();
        $original_name = $file->getClientOriginalName();

        $name = basename($original_name, '.'.$extension);

        return md5($name.time()) . '.' . $extension;
    }

}