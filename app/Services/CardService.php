<?php

namespace App\Services;

class CardService {

    const OSMICARD_API_ID = 'KYNX9TC844A4MNESYNLN';
    const OSMICARD_API_KEY = 'a6e1bef6bd3905a2166d6bee4aa80a7a652f2666';
    const WARRANTY_FIELDS = ['ДАТА УСТАНОВКИ', 'ВЛАДЕЛЕЦ', 'МАСТЕР', 'МАРКА', 'ГОС.НОМЕР', 'НОМЕР ПАРТИИ', 'ПРОДУКТ', 'РЕГИСТРАЦИЯ ГАРАНТИИ'];
    const REGISTER_WARRANTY_FIELD = 'РЕГИСТРАЦИЯ ГАРАНТИИ';
    public function __construct()
    {
       $this->updateOsmicardToken();
    }

    public function curlRequest($url, $method = 'GET', Array $args = [])
    {

        $token = session('osm_token');

        $token_access = [
            'apiId' => self::OSMICARD_API_ID,
            'apiKey' => self::OSMICARD_API_KEY,
        ];
        if ($args) {
            $post_fields = $args + $token_access;
        } else {
            $post_fields = $token_access;
        }

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://api9.osmicards.com/v2t/' . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => json_encode($post_fields, JSON_UNESCAPED_UNICODE),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $token
            ),
        ]);
        $response = json_decode(curl_exec($curl), true);

        curl_close($curl);

        return $response;
    }

    public function updateOsmicardToken()
    {

        $response = $this->curlRequest('getToken', 'POST');

        session(['osm_token' => $response['token']]);

        return $response;
    }

    public function getOsmicardListPasses($template = '', Array $fields = [], $active = false)
    {
        $fields_param = $template_param = $active_param = '';
        if ($fields) {
            $fields_param = str_replace(' ', '+', '&fields='.implode(',', $fields));
        }
        if ($template)
            $template_param = '&template='.$template;

        if ($active)
            $active_param = '&activeOnly=true';

        $response = $this->curlRequest('passes?stats=true&status=true&fields='.$fields_param.$template_param.$active_param, 'GET');

        return $response;
    }

    public function getOsmicardListPassesNumbersOnly($template = '', $page = 1)
    {
        $template_param = '?page='.$page;
        if ($template)
            $template_param = '&template='.$template;



        $response = $this->curlRequest('passes'.$template_param, 'GET');

        return $response;
    }

    public function getOsmicardListTemplates()
    {
        $response = $this->curlRequest('templates', 'GET');

        return $response;
    }

    public function getOsmicardTemplate($name)
    {
        $response = $this->curlRequest('templates/'.$name.'?showKeys=true', 'GET');

        return $response;
    }

    public function getOsmicardTemplateFields($name)
    {
        $response = $this->curlRequest('templates/'.$name.'?showKeys=true', 'GET');

        $r = array_column($response['values'], 'label');
        return $r;
    }

    public function getSingleOsmicard($number)
    {
        $response = $this->curlRequest('passes/'.$number.'?stats=true', 'GET');

        return $response;
    }

    public function getSingleOsmicardWarrantyFields($number)
    {
        $response = $this->getSingleOsmicard($number);
        $needed_array = [];
        if (!empty($response['values']) && count($response['values']) > 0) {
            foreach ($response['values'] as $item) {
                if (in_array($item['label'], self::WARRANTY_FIELDS)) {
                    $needed_array[$item['label']] = $item['value'];
                }
            }
        }

        return $needed_array;
    }

    public function getSingleOsmicardWarrantyEvent($number)
    {
        $response = $this->getSingleOsmicard($number);
        if (!empty($response['values']) && count($response['values']) > 0) {
            foreach ($response['values'] as $item) {
                if ($item['label'] === self::REGISTER_WARRANTY_FIELD) {
                    return $item['value'];
                }
            }
        }

        return [];
    }

    public function getLinkOsmicard($number, String $format = 'url')
    {
        $response = $this->curlRequest('passes/'.$number.'/link?type='.$format, 'GET');

        return $response;
    }

    public function createSingleOsmicard($number, $template, $args)
    {
        $response = $this->curlRequest('passes/'.$number.'/'.$template.'?barcode=true&withValues=true', 'POST', $args);

        return $response;
    }

    public function deleteSingleOsmicard($number)
    {
        $response = $this->curlRequest('passes/'.$number, 'DELETE');

        return $response;
    }

    public function getFieldsLabelsTemplateOsmicard($template)
    {
        $data = $this->getOsmicardTemplate($template);
        $fields = [];

        if (!empty($data['values']) && is_array($data['values'])) {
            foreach ($data['values'] as $val) {
                $fields[] = $val['label'];
            }
        }

        return $fields;
    }

    public function updateWarrantyEventSingleOsmicard($number, $string = '')
    {
        if (!$number || !$string)
            return;

        $registered_card_event_value = $this->getSingleOsmicardWarrantyEvent($number);

        if ($registered_card_event_value == '-empty-') {
            $updated_card_event_value = $string;
        } else {
            $updated_card_event_value = $registered_card_event_value . $string;
        }


        $args = [];
        $args['values'] = [
            [
                'label' => self::REGISTER_WARRANTY_FIELD,
                'value' => $updated_card_event_value
            ]
        ];

        $response = $this->curlRequest('passes/'.$number, 'PUT', $args);

        return $response;
    }

    public function countSentSMS($card)
    {
        $notifications = $card->item->order->user->notifications;
        $filtered_notifications = array_filter($notifications->toArray(), function ($var) use ($card) {
            return (isset($var['data']['card']) && $var['data']['card'] == $card->number);
        });
        return count($filtered_notifications);
    }

    public function masters()
    {

    }
}
