<?php

namespace App\Observers;

use App\Order;
use Carbon\Carbon;

class OrderObserver
{
    /**
     * Handle the order "created" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        //
    }

    /**
     * Handle the order "updated" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
        $prev_status = $order->getOriginal('status');
        $dataToUpdate = [];

        if ( $prev_status === 'in_process' && in_array($order->status, ['completed', 'completed_not_happy']) ) {
            $dataToUpdate['completed_datetime_end'] = Carbon::now()->format('Y-m-d H:i:s');
        }

        if (in_array($prev_status, ['completed', 'completed_not_happy']) && in_array($order->status, ['released', 'released_not_happy'])) {
            $dataToUpdate['released_datetime_end'] = Carbon::now()->format('Y-m-d H:i:s');
        }

        if (!empty($dataToUpdate)) {
            Order::where('id', $order->id)->update($dataToUpdate);
        }
    }

    /**
     * Handle the order "deleted" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the order "restored" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the order "force deleted" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
