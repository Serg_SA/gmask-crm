<?php

namespace App\Observers;

use App\Agent;
use App\User;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        $role = $user->role->name ?? '';
        if ($role == 'agent') {

            $user->agent()->create([
                'contact' => ''
            ]);

            $user->save();
        }
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        $role = $user->role->name ?? '';
        if ($role == 'agent') {

            $agent = Agent::where('user_id', $user->id)->first();
            if ( !$agent ) {
                $user->agent()->create([
                    'contact' => ''
                ]);
                $user->save();
            }
        }
    }

//    /**
//     * Handle the user "deleted" event.
//     *
//     * @param  \App\User  $user
//     * @return void
//     */
//    public function deleted(User $user)
//    {
//        $role = $user->role->name ?? '';
//        if ($role == 'agent') {
//
//            $user->agent()->create([
//                'contact' => ''
//            ]);
//
//            $user->save();
//        }
//    }
}
