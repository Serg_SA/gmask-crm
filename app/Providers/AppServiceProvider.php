<?php

namespace App\Providers;

use App\FormFields\DatetimeFormField;
use App\Observers\UserObserver;
use App\Observers\OrderObserver;
use App\Order;
use App\User;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Facades\Voyager;
use Jenssegers\Date\Date;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Voyager::addFormField(DatetimeFormField::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Date::setlocale(config('app.locale'));
        User::observe(UserObserver::class);
        Order::observe(OrderObserver::class);
    }
}
