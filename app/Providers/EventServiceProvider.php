<?php

namespace App\Providers;

use App\Events\CardCreated;
use Illuminate\Auth\Events\Registered;
use App\Events\OrderCreated;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        \TCG\Voyager\Events\BreadDataAdded::class => [
            \App\Listeners\AddNewUserVoyager::class,
            \App\Listeners\CreateNewOrder::class,
//            \App\Listeners\CreateNewCard::class,
        ],
//        CardCreated::class => [
//            \App\Listeners\CreateNewCard::class,
//        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
