<?php

namespace App\Providers;

use App\Services\CardService;
use Illuminate\Support\ServiceProvider;

class OsmicardServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Osmicard', CardService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
