<?php

namespace App\Providers;

use App\Services\DropzoneService;
use Illuminate\Support\ServiceProvider;

class DropzoneServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Dropzone', DropzoneService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
