<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function category()
    {
        return $this->belongsToMany('App\Category');
    }

    public function cardTemplate()
    {
        return $this->belongsTo('App\CardTemplate');
    }
}
