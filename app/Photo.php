<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'path',  'filename',  'size', 'mime_type',
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

}
