<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'qty', 'notice', 'price', 'service_id', 'order_id'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function card()
    {
        return $this->hasOne('App\Card');
    }

}
