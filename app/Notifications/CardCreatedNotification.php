<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\NexmoMessage;

class CardCreatedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The message to send.
     *
     * @var string
     */
    public $message;

    /**
     * Card_number.
     *
     * @var string
     */
    public $card_number;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message, $card_number)
    {
        $this->message = $message;
        $this->card_number = $card_number;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo', 'database'];
    }

    /**
     * Get the sms representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmo($notifiable)
    {
        return (new NexmoMessage)
            ->content($this->message)
            ->unicode();
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'card' => $this->card_number
        ];
    }
}
