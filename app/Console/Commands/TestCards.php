<?php

namespace App\Console\Commands;

use App\Facades\Osmicard;
use Illuminate\Console\Command;

class TestCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'osmicard:find {query}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get test cards';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $query = $this->argument('query');
        $page = 1;
        $response = Osmicard::getOsmicardListPassesNumbersOnly('', $page);
        if ( empty($response['cards']) ) {
            echo 'Not found cards'.PHP_EOL;
            return '';
        }

        $all_cards = $response['cards'];
        while (count($response['cards']) > 999) {
            $page++;
            $response = Osmicard::getOsmicardListPassesNumbersOnly('', $page);
            $all_cards = array_merge($all_cards, $response['cards']);
        }

        $filtered_cards = array_values( array_filter($all_cards, function($value) use ($query) {
            return strpos($value, $query) !== false;
        }) );

        foreach ($filtered_cards as $val) {
            echo $val.PHP_EOL;
        }
    }
}
