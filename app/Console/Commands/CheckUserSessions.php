<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class CheckUserSessions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sessions:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check user sessions and display last activity';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Получение списка файлов сессий
        $sessionFiles = Storage::disk('sessions')->files();

        // Проверка наличия файлов
        if (empty($sessionFiles)) {
            $this->info('No session files found.');
            return Command::SUCCESS;
        }

        $authenticatedSessions = [];

        foreach ($sessionFiles as $sessionFile) {
            // Пропуск файлов с расширениями
            if (str_contains($sessionFile, '.')) {
                $this->info("Skipping non-session file: $sessionFile");
                continue;
            }

            try {
                // Чтение содержимого файла сессии
                $sessionContent = Storage::disk('sessions')->get($sessionFile);

                // Попытка расшифровать и десериализовать содержимое файла сессии
                $decryptedContent = Crypt::decrypt($sessionContent);
                $sessionData = unserialize($decryptedContent);

                // Проверка наличия идентификатора пользователя в данных сессии
                if (isset($sessionData['user_id'])) {
                    $authenticatedSessions[] = [
                        'session_file' => $sessionFile,
                        'user_id' => $sessionData['user_id'],
                        'session_data' => $sessionData
                    ];
                }
            } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
                $this->error("Could not decrypt session file: $sessionFile");
            } catch (\Exception $e) {
                $this->error("Could not unserialize session file: $sessionFile");
            }
        }

        // Вывод информации о авторизованных сессиях
        if (!empty($authenticatedSessions)) {
            foreach ($authenticatedSessions as $session) {
                $this->info("Session File: {$session['session_file']}");
                $this->info("User ID: {$session['user_id']}");
                $this->info("Session Data: " . print_r($session['session_data'], true));
            }
        } else {
            $this->info('No authenticated user sessions found.');
        }

        return Command::SUCCESS;
    }
}
