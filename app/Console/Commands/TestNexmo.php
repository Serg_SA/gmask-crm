<?php

namespace App\Console\Commands;

use App\Card;
use App\Facades\Osmicard;
use App\Notifications\CardCreatedNotification;
use App\Notifications\OrderCreatedNotification;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class TestNexmo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Specific user send test sms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $user = User::where('phone', '+380632200525')->first();
//        print_r(count($user->notifications));
//        foreach ($user->notifications as $notification) {
//            echo $notification->notifiable_id;
//        }

//        $user = User::where('phone', '+380632200525')->first();
        $card = Card::where('number', 'TESTD80632200525')->first();
//        dd($card);
        $r = Osmicard::countSentSMS($card);


//        $card_number = 'TESTA80632200525';
//        $link = Osmicard::getLinkOsmicard($card_number);
//        if (empty($link['link']))
//            return;
//
//        $msg = 'GMASK: гарантийная карта: ' . $link['link'];
//        Notification::send($user, new CardCreatedNotification($msg, $card_number));
    }
}
