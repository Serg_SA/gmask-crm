<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;

class OrdersRealDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:realdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set real date to date in orders table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orders = Order::all();
        foreach ($orders as $order) {
            $statuses_completed = ['completed', 'completed_not_happy'];
            $statuses_released = ['released', 'released_not_happy'];
            if (in_array($order->status, $statuses_completed)) {
                $order->completed_datetime_end = $order->datetime_end;
            }
            if (in_array($order->status, $statuses_released)) {
                $order->completed_datetime_end = $order->datetime_end;
                $order->released_datetime_end = $order->datetime_end;
            }
            $order->save();
        }
        return 0;
    }
}
