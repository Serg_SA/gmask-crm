<?php

namespace App\Listeners;

use App\Events\OrderCreated;
use App\Notifications\OrderCreatedNotification;
use App\Order;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Support\Facades\Notification;
use Jenssegers\Date\Date;
use TCG\Voyager\Events\BreadDataAdded;

class CreateNewOrder
{
    const PRELIMINARY_NOTIFY_TIME_DEFAULT = 120;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param BreadDataAdded $event
     * @return void
     */
    public function handle(BreadDataAdded $event)
    {
        if( $event->dataType->name === 'orders' ) {
            $user = User::findOrFail($event->data->user->id);
            $order = Order::findOrFail($event->data->id);
            $delay = $this->delay($event);

            $msg = $this->message($event);
            if ($msg && !$order->notified) {
                Notification::send($user, new OrderCreatedNotification($msg, $delay));
                $order->notified = 1;
                $order->save();
            }
        }
    }

    /**
     * Get minutes
     *
     * @return integer
     */
    private function minutes(BreadDataAdded $event)
    {
        $datetime_now = Carbon::now();
        $datetime_start = Carbon::parse($event->data->datetime_start);

        $diff = $datetime_now->diffInMinutes($datetime_start);

        return $datetime_start->isPast() ? -$diff : $diff;
    }

    /**
     * Get delay for Queue
     *
     * @return integer
     */
    private function delay(BreadDataAdded $event)
    {
        $diff_minutes = $this->minutes($event);

        $preliminary_notify_time = ( setting('admin.preliminary_notify_time') && is_numeric(setting('admin.preliminary_notify_time')) )
                ? setting('admin.preliminary_notify_time')
                : self::PRELIMINARY_NOTIFY_TIME_DEFAULT;

        $delay = $diff_minutes > $preliminary_notify_time ? $diff_minutes - $preliminary_notify_time : 0;

        return $delay;
    }

    /**
     * Generate message
     *
     * @return string | false
     */
    private function message(BreadDataAdded $event)
    {
        $date_start = Date::parse($event->data->datetime_start)->format('d.m');
        $time_start = Date::parse($event->data->datetime_start)->format('H:i');
        $address = setting('admin.address');
        $phone = setting('admin.phone');
        $receiver_name = $event->data->user->name;

        if ( !$date_start || !$time_start || !$address || !$phone || !$receiver_name )
            return false;

        $message = "Уважаемая(ый) $receiver_name! $date_start в $time_start вы записаны в GMASK DETAILING по адресу $address. Вопросы по тел: $phone";

        return $message;
    }
}
