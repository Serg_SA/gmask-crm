<?php

namespace App\Listeners;

use App\Notifications\RegistrationUserNotification;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use TCG\Voyager\Events\BreadDataAdded;

class AddNewUserVoyager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BreadDataAdded  $event
     * @return void
     */
    public function handle(BreadDataAdded $event)
    {
        if( $event->dataType->name === 'users' ) {
            $user = User::find($event->data->id);
            event(new Registered($user));

            Notification::send($user, new RegistrationUserNotification());
        }

    }
}
