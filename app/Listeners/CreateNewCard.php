<?php

namespace App\Listeners;

use App\Facades\Osmicard;
use App\Notifications\CardCreatedNotification;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use TCG\Voyager\Events\BreadDataAdded;
use App\Events\CardCreated;

class CreateNewCard
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CardCreated  $event
     * @return void
     */
    public function handle(CardCreated $event)
    {
        $card = $event->card;
        $user = $card->item->order->user ?? null;
        $phone = $user->phone ?? null;

        if (!$card->number || !$user || !$phone)
            return;

        $link = Osmicard::getLinkOsmicard($card->number);
        if (empty($link['link']))
            return;

        $msg = 'GMASK. Гарантийная карта: ' . $link['link'];
        Notification::send($user, new CardCreatedNotification($msg));
    }
}
