<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentCard extends Model
{
    protected $fillable = ['number', 'phone_number', 'meters'];

    /**
     * Return Agent.
     */
    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }

    /**
     * Return Order.
     */
    public function agent_order()
    {
        return $this->belongsTo('App\AgentOrder');
    }
}
