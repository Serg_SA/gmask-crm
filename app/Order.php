<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'completed_datetime_end',
        'released_datetime_end',
    ];

    public function getDatetimeStartAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function getDatetimeEndAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function getCompletedDatetimeEndAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('Y-m-d H:i:s') : $value;
    }

    public function getReleasedDatetimeEndAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('Y-m-d H:i:s') : $value;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function master()
    {
        return $this->belongsTo('App\User', 'master_id');
    }

    public function items()
    {
        return $this->hasMany('App\Item');
    }

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }
}
