<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [
        'name'
    ];

    public function cardTemplate()
    {
        return $this->belongsTo('App\CardTemplate');
    }
}
