<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use TCG\Voyager\Models\Role;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Return User Cars.
     */
    public function cars()
    {
        return $this->belongsToMany('App\Car')->withPivot([
            'id', 'number', 'updated_at', 'created_at'
        ]);
    }

    /**
     * Return User Orders.
     */
    public function orders()
    {
        return $this->belongsToMany('App\Order');
    }

    /**
     * Return User Role.
     */
    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    /**
     * Return Agent.
     */
    public function agent()
    {
        return $this->hasOne('App\Agent');
    }

    /**
     * Return Users with role "master".
     */
    public function scopeMaster($query)
    {
        $role_master = Role::where('name', 'master')->first();

        return $query->where('role_id', $role_master->id);
    }

    /**
     * Return Users with role "client".
     */
    public function scopeClient($query)
    {
        $role_client = Role::where('name', 'client')->first();

        return $query->where('role_id', $role_client->id);
    }

    /**
     * Route notifications for the Nexmo channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForNexmo($notification)
    {
        return $this->phone;
    }


    public function isVerified()
    {
        return $this->email_verified_at ? true : false;
    }
}
