<?php

namespace App\FormFields;

use TCG\Voyager\FormFields\AbstractHandler;

class DatetimeFormField extends AbstractHandler
{
    protected $codename = 'datetime';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('vendor.voyager.formfields.datetime', [
            'row' => $row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent
        ]);
    }
}