<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contact', 'is_approved'
    ];

    /**
     * Return User.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Return Cards.
     */
    public function cards()
    {
        return $this->hasMany('App\AgentCard');
    }

    /**
     * Return Orders.
     */
    public function orders()
    {
        return $this->hasMany('App\AgentOrder');
    }
}
