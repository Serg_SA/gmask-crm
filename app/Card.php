<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = ['number'];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }
}
