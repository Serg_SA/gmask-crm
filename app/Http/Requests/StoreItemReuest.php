<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreItemReuest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qty' => 'required|numeric|min:1|max:1000',
            'notice' => 'max:1000',
            'price' => 'required|numeric|min:1|max:3000000',
            'order_id' => 'required|numeric',
            'service_id' => 'required|numeric',
        ];
    }
}
