<?php

namespace App\Http\Controllers;

use App\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AgentController extends Controller
{
    /**
     * Update is_approved via AJAX.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approveAjax(Request $request)
    {
        $rules = [
            'agent_id' => 'numeric',
            'is_approved' => 'boolean',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ( $validator->fails() ) {
            $response = [
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ];
        } else {
            $agent = Agent::find($request->agent_id);
            $agent->is_approved = $request->is_approved;
            $agent->save();

            $response = [
                'success' => true,
                'errors' => 'ok'
            ];

        }

        return response()->json($response);
    }
}
