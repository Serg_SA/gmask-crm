<?php

namespace App\Http\Controllers;

use App\AgentCard;
use App\AgentOrder;
use App\Card;
use App\Facades\Osmicard;
use App\Notifications\CardCreatedNotification;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Vonage\Client;
use Vonage\Client\Credentials\Basic;

class AgentCardController extends Controller
{
    /**
     * Show the all agent cards.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $cards = AgentCard::where('agent_id', '=', $user->agent->id)->paginate(10);
        return view('agents.cards', ['cards' => $cards, 'is_approved' => $user->agent->is_approved]);
    }

    /**
     * Store card via AJAX.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAjax(Request $request)
    {
        $user = Auth::user();
        $rules = [
            'full_name' => 'required',
            'master' => 'required',
            'phone_number' => 'required',
            'car_brand_model' => 'required',
            'car_number' => 'required',
            'meters' => 'numeric|required',
            'order_number' => 'required',
            'warranty_template' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ( $validator->fails() ) {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        // Templates field cases
        $values = [];
        $values[] = [
            'label' => 'ДАТА УСТАНОВКИ',
            'value' => date('Y-m-d'),
        ];
        $values[] = [
            'label' => 'ВЛАДЕЛЕЦ',
            'value' => $request->full_name,
        ];
        $values[] = [
            'label' => 'МАСТЕР',
            'value' => $request->master,
        ];
        $values[] = [
            'label' => 'МАРКА',
            'value' => $request->car_brand_model,
        ];
        $values[] = [
            'label' => 'ГОС.НОМЕР',
            'value' => $request->car_number,
        ];
        $values[] = [
            'label' => 'ПРОДУКТ',
            'value' => $request->warranty_template ,
        ];
        $values[] = [
            'label' => 'НОМЕР ПАРТИИ',
            'value' => $request->order_number,
        ];
        $values[] = [
            'label' => 'КОНТАКТЫ',
            'value' => $user->agent->contact,
        ];

        $args['values'] = $values;

        $created_card_number = '';
        $number = substr($request->phone_number, 2);
        foreach ( range('A', 'Z') as $letter ) {
            $prefix = env('OSMICARD_NUMBER_PREFIX_AGENT', 'C');
            $response = Osmicard::createSingleOsmicard($prefix.$letter.$number, $request->warranty_template, $args);

            if ( empty($response['RCODE']) && !empty($response['general']['serialNo']) && $response['general']['serialNo'] === $prefix.$letter.$number) {
                $created_card_number = $response['general']['serialNo'];
                break;
            }
        }

        $agent_order = AgentOrder::firstWhere('number', $request->order_number);
        if ( $created_card_number && $agent_order ) {
            $card = $user->agent->cards()->create([
                'number' => $created_card_number,
                'phone_number' => $request->phone_number,
                'meters' => $request->meters,
            ]);

            $agent_order->meters_left = $agent_order->meters_left - $request->meters;
            $agent_order->save();

            $card->agent_order()->associate($agent_order);
            $card->save();
//            event(new CardCreated($card));

            return response()->json(['success' => true, 'card' => $created_card_number], 200);
        }

        return response()->json(['error' => 'К сожалению карта не была создана'], 200);
    }

    /**
     * Update warranty event agent card via AJAX.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateEventAjax(Request $request)
    {
        $rules = [
            'warranty_event' => 'required',
            'number' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ( $validator->fails() ) {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        $string = '<div>' . date('Y-m-d') . ': ' . $request->warranty_event . '</div>';
        $card = Osmicard::updateWarrantyEventSingleOsmicard($request->number, $string);

        if (!empty($card['general']['serialNo'])) {
            $new_event_val = Osmicard::getSingleOsmicardWarrantyEvent($card['general']['serialNo']);
            return response()->json(['success' => true, 'events' => $new_event_val], 200);
        } else {
            return response()->json([
                'success' => false,
                'errors' => 'Card was not updated'
            ]);
        }

    }

    /**
     * Get warranty agent card via AJAX.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCardAjax(Request $request)
    {
        $rules = [
            'number' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ( $validator->fails() ) {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        $card_fields = Osmicard::getSingleOsmicardWarrantyFields($request->number);

        if ( count($card_fields) > 0 ) {
            return response()->json(['success' => true, 'fields' => $card_fields], 200);
        } else {
            return response()->json([
                'success' => false,
                'errors' => 'Card was not updated'
            ]);
        }

    }

    /**
     * Send SMS via AJAX.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendSMSAjax(Request $request)
    {
        $link = Osmicard::getLinkOsmicard($request->number);
        $response_data = [];
        if (!empty($link['link'])) {
            try {

                $basic  = new Basic(getenv("NEXMO_KEY"), getenv("NEXMO_SECRET"));
                $client = new Client($basic);

                $agent_card = AgentCard::where('number', $request->number)->first();

                $receiverNumber = $agent_card->phone_number;
                $message = env('NEXMO_BRAND_NAME') . '. Warranty Card: ' . $link['link'];

                $message = $client->message()->send([
                    'to' => $receiverNumber,
                    'from' => 'Vonage APIs',
                    'text' => $message
                ]);
                $response_data = [
                    'success' => true,
                    'msg' => __('generic.sms_was_sent_successfully'),
                ];

            } catch (Exception $e) {
                $response_data = [
                    'success' => false,
                    'msg' => $e->getMessage(),
                ];
            }

        } else {
            $response_data = [
                'success' => false,
                'msg' => __('generic.sms_not_sent'),
            ];
        }

        return response()->json($response_data, 200);
    }

    /**
     * Get QR code via AJAX.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getQRCodeAjax(Request $request)
    {
        $link = Osmicard::getLinkOsmicard($request->number, 'qr');

        if (!empty($link['link'])) {
            return response()->json([
                'success' => true,
                'link' => $link['link']], 200);
        } else {
            return response()->json([
                'success' => false,
                'errors' => 'Card was not found'
            ], 404);
        }
    }
}
