<?php

namespace App\Http\Controllers;

use App\Field;
use Illuminate\Http\Request;

class FieldController extends Controller
{
    /**
     * Store field via AJAX.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAjax(Request $request)
    {
        return response()->json(array('success' => true), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Field::findOrFail($id);
        $deleted_success = $item->delete();

        if ($deleted_success) {
            return back()->with([
                'message'    => __('voyager::generic.successfully_deleted'),
                'alert-type' => 'success',
            ]);
        } else {
            return back()->with([
                'message'    => __('voyager::generic.error_deleting'),
                'alert-type' => 'error',
            ]);
        }
    }
}
