<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreItemReuest;
use App\Http\Requests\UpdateItemRequest;
use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreItemReuest $request)
    {
        $data = $request->validated();

        Item::create([
            'qty' => $data['qty'],
            'notice' => $data['notice'],
            'price' => $data['price'] ?? null,
            'order_id' => $data['order_id'],
            'service_id' => $data['service_id'],
        ]);

        return redirect()->back()->with([
            'message'    => __('voyager::generic.successfully_created'),
            'alert-type' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::findOrFail($id);
        $deleted_success = $item->delete();

        if ($deleted_success) {
            return back()->with([
                'message'    => __('voyager::generic.successfully_deleted'),
                'alert-type' => 'success',
            ]);
        } else {
            return back()->with([
                'message'    => __('voyager::generic.error_deleting'),
                'alert-type' => 'error',
            ]);
        }
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateAjax(UpdateItemRequest $request)
    {
        $data = $request->validated();

        $item = Item::findOrFail($data['item_id']);
        $item_updated = $item->update($data);

        if (!$item_updated) {
            return redirect()->back()->with([
                'message'    => __('voyager::generic.update_failed'),
                'alert-type' => 'error',
            ]);
        } else {
            return redirect()->back()->with([
                'message'    => __('voyager::generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
        }
    }
}