<?php

namespace App\Http\Controllers;

use App\Code;
use App\Phone;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Nexmo\Laravel\Facade\Nexmo;

class VerifyController extends Controller
{
    public function show(Request $request) {
        return view('verify');
    }

    public function send($phone)
    {
//        if (User::where('phone', $phone)->exists()) {
//            return response()->json([
//                'error' => __('generic.this_number_already_exist')
//            ]);
//        }

        $request = new \Vonage\Verify\Request($phone, env('NEXMO_BRAND_NAME'));
        $verification = Nexmo::verify()->start($request);

        if ($verification) {
            Phone::updateOrCreate(
                ['phone' => $phone],
                ['request_id' => $verification->getRequestId(), 'is_verified' => false]
            );

            return response()->json([
                'success' => __('generic.sms_was_sent_successfully')
            ]);
        }

        return response()->json([
            'error' => __('generic.sms_not_sent')
        ]);

    }

    public function check($code) {
        return response()->json([
            'success' => __('generic.phone_number_is_verified'),
        ]);
        if (strlen($code) === 4 && is_numeric($code)) {
            try {
                $phone = $_POST['phone'];
                $phone_model = Phone::where('phone', $phone)->first();

                if ( $phone_model !== null) {
                    Nexmo::verify()->check(
                        $phone_model->request_id,
                        $code
                    );
                    $phone_model->is_verified = true;
                    $phone_model->save();

                    return response()->json([
                        'success' => __('generic.phone_number_is_verified'),
                    ]);
                }
                return response()->json([
                    'error' => 'request_id is not found in DB'
                ]);
//                request()->session()->forget('verify:request_id');
            } catch (Nexmo\Client\Exception\Request $e) {
                return response()->json([
                    'error' => $e->getMessage()
                ]);
            }
        }


    }
}
