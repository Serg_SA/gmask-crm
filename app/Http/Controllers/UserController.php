<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\StoreCarUserRequest;
use App\Http\Requests\StoreUserAjaxRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request)
    {
        return $request->user();
    }

    /**
     * Store user via AJAX.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAjax(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'sometimes|email',
            'phone' => 'required|unique:users',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ( $validator->fails() ) {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->phone . '_' . $request->phone)
        ]);

        DB::table('users')
            ->where('id', $user->id)
            ->update(['settings' => json_encode(['locale' => 'ru'])]);

        return response()->json(array('success' => true), 200);
    }


    /**
     * Receive user cars.
     *
     * @param  \App\User id - $id
     * @return \Illuminate\Http\Response
     */
    public function select2()
    {
        if (isset($_POST['search'])) {
            $users = User::client()->where('phone', 'LIKE', '%'.$_POST['search'].'%')->orWhere('name', 'LIKE', '%'.$_POST['search'].'%')->get();
        } else {
            $users = User::client()->get();
        }

        $results = [];
        foreach ($users as $user) {
            $results[] = [
                'id'   => $user->id,
                'text' => $user->phone . ' (' . $user->name . ')',
            ];
        }

        return response()->json([
            'results' => $results
        ]);

    }

}
