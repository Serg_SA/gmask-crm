<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AgentDashboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user = Auth::user();
        return view('agents.dashboard', [
            'is_approved' => $user->agent->is_approved,
            'orders' => $user->agent->orders ?? [],
        ]);
    }

    public function cards() {
        $user = Auth::user();
        return view('agents.cards', $user);
    }
}
