<?php

namespace App\Http\Controllers;

use App\Order;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Facades\Order as OrderService;

class ReportController extends Controller
{
    public function index()
    {
        return view('admin.report.index');
    }

    public function search(Request $request)
    {
        if (!$request->date_between)
            return ['error' => 'Дата не указана'];

        $pattern = '/(\d{4}-\d{2}-\d{2})(?:\s*—\s*(\d{4}-\d{2}-\d{2}))?/';

        preg_match($pattern, $request->date_between, $matches);

        if (!empty($matches)) {
            $start_date = $matches[1] . ' 00:00:00';
            $end_date = ($matches[2] ?? $matches[1]) . ' 23:59:59';
        } else {
            return ['error' => 'Дата выбрана некоректно'];
        }

        $statuses = $request->status;
        $in_process_status = array_intersect($statuses, ['in_process']);
        $completed_statuses = array_intersect($statuses, ['completed', 'completed_not_happy']);
        $released_statuses = array_intersect($statuses, ['released', 'released_not_happy']);

        $orders = Order::with(['user', 'master', 'items', 'items.service'])
            ->where(function ($query) use ($start_date, $end_date, $in_process_status, $completed_statuses, $released_statuses) {
                if (!empty($in_process_status)) {
                    $query->where(function ($q) use ($start_date, $end_date, $in_process_status) {
                        $q->whereIn('status', $in_process_status)
                            ->whereBetween('datetime_end', [$start_date, $end_date]);
                    });
                }

                if (!empty($completed_statuses)) {
                    $query->orWhere(function ($q) use ($start_date, $end_date, $completed_statuses) {
                        $q->whereIn('status', $completed_statuses)
                            ->whereBetween('completed_datetime_end', [$start_date, $end_date]);
                    });
                }

                if (!empty($released_statuses)) {
                    $query->orWhere(function ($q) use ($start_date, $end_date, $released_statuses) {
                        $q->whereIn('status', $released_statuses)
                            ->whereBetween('released_datetime_end', [$start_date, $end_date]);
                    });
                }
            })
            ->orderBy('datetime_end', 'asc')
            ->distinct()
            ->get()
            ->map(function ($order) {
                $order->edit_link = route('voyager.orders.edit', ['id' => $order->id]);
                $order->status_label = __('generic.' . $order->status);
                $order->total_price = OrderService::getTotalPrice($order->id);
                $order->datetime_start_formated = Carbon::parse($order->datetime_start)->isoFormat('<b>D MMM</b><br><b>Y hh:mm</b>');
                $order->datetime_end_formated = Carbon::parse($order->datetime_end)->isoFormat('<b>D MMM</b><br> <b>Y hh:mm</b>');
                $order->completed_datetime_end_formated = $order->completed_datetime_end
                    ? Carbon::parse($order->completed_datetime_end)->isoFormat('<b>D MMM</b><br> <b>Y hh:mm</b>')
                    : '–';
                $order->released_datetime_end_formated = $order->released_datetime_end
                    ? Carbon::parse($order->released_datetime_end)->isoFormat('<b>D MMM</b><br> <b>Y hh:mm</b>')
                    : '–';
                if ($order->completed_datetime_end) {
                    $datetime_end = Carbon::parse($order->datetime_end)->startOfDay();
                    $completed_datetime_end = Carbon::parse($order->completed_datetime_end)->startOfDay();
                    $completed_days_difference = $datetime_end->diffInDays($completed_datetime_end);
                    if ($completed_datetime_end->isAfter($datetime_end)) {
                        $overdue = true;
                    } elseif ($completed_datetime_end->isBefore($datetime_end)) {
                        $overdue = false;
                    }
                    if ($completed_days_difference) {
                        if ($overdue) {
                            $order->completed_diff = '<span class="overdue">прсрч. ' . $completed_days_difference . ' д</span>';
                        } else {
                            $order->completed_diff = '<span class="early">доср. ' . $completed_days_difference . ' д</span>';
                        }
                    }
                } else {
                    $datetime_end = Carbon::parse($order->datetime_end)->startOfDay();
                    $current_datetime = Carbon::now()->startOfDay();
                    if ($current_datetime->isAfter($datetime_end)) {
                        $order->in_process_diff = '<span class="overdue">прсрч. ' . $datetime_end->diffInDays($current_datetime) . ' д</span>';
                    }
                }

                return $order;
            });

        $orders_total = 'Общая сумма: <span>' . OrderService::getTotalPriceManyOrders($orders) . '</span>';

        return response()->json(compact('orders', 'orders_total'));
    }
}
