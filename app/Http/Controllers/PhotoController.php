<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use App\Facades\Dropzone;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    public function store(Request $request)
    {
//        $data = $request->validated();
//        return response()->json($request->toArray()['file']);
        if ( $imageName = Dropzone::generateName($request->file) ) {

            $manager = new ImageManager();

            $image = $manager->make($request->file)->resize(1900, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $image->save(storage_path('app/public/cars/'.$imageName));

            $public_path = 'cars/'.$imageName;
            $data_photo = [
                'order_id' => $request->order_id,
                'path' => $public_path,
                'filename' => $imageName,
                'size' => Storage::disk('public')->size($public_path),
                'mime_type' => Storage::disk('public')->mimeType($public_path),
            ];

            Photo::create($data_photo);

        }

        return response()->json(['name' => $imageName]);
    }

    public function destroy($id)
    {
        $photo = Photo::find($id);
        $deleted = Storage::disk('public')->delete('cars/'.$photo->filename);
        $photo->delete();

        return response()->json(['deleted' => $deleted]);


    }
}
