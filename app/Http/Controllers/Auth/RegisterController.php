<?php

namespace App\Http\Controllers\Auth;

use App\Agent;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Role;
use App\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'regex:'.config('services.nexmo.phone_number_pattern')],
            'address' => ['required', 'string'],
            'company' => ['required', 'string'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $role = Role::where('name', 'agent')->first();

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => Hash::make($data['password']),
        ]);

        $user->role()->associate($role);
        $user->save();

        $contact = __('generic.company') . ': "' . $data['company'] . '" | ';
        $contact .= __('generic.phone_number') . ': ' . $data['phone'] . ' | ';
        $contact .= __('generic.address') . ': ' . $data['address'];

        $agent = $user->agent;
        $agent->contact = $contact;
        $agent->save();

        return $user;
    }

//    public function authenticated(Request $request, Authenticatable $user)
//    {
//        $request->session()->put('verify:user:id', $user->id);
//        $verification = Nexmo::verify()->start([
//            'number' => $user->phone,
//            'brand'  => 'Laravel Demo'
//        ]);
//        $request->session()->put('verify:request_id', $verification->getRequestId());
//        return redirect('verify');
//    }
}
