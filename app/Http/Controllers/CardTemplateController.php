<?php

namespace App\Http\Controllers;

use App\Facades\Osmicard;
use Illuminate\Http\Request;

class CardTemplateController extends Controller
{
    public function fields(Request $request)
    {
        $search = $request->input('search', false);
        $template = $request->input('template', false);

        $template_feilds = Osmicard::getOsmicardTemplateFields($template);
        $template_feilds_collection = collect($template_feilds);

        if ($search) {
            $filtered_fields = $template_feilds_collection->filter(function ($model) use ($search) {
                return mb_stripos($model, $search) !== false;
            });
        } else {
            $filtered_fields = $template_feilds_collection;
        }

        $results = [];
        foreach ($filtered_fields as $field) {
            $results[] = [
                'name'   => $field,
            ];
        }

        return response()->json([
            'results' => $results
        ]);

    }
}
