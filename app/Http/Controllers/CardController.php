<?php

namespace App\Http\Controllers;

use App\Card;
use App\Events\CardCreated;
use App\Events\OrderCreated;
use App\Facades\Osmicard;
use App\Http\Requests\StoreCardRequest;
use App\Item;
use App\Notifications\CardCreatedNotification;
use App\Services\CardService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class CardController extends Controller
{
    /**
     * Store card via AJAX.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAjax(Request $request)
    {
        $rules = [
            'full_name' => 'required',
            'master' => 'required',
            'car_number' => 'required',
            'roll_number' => '',
            'item_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ( $validator->fails() ) {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        $item = Item::find($request->item_id);
        $phone = $item->order->user->phone;
        $template = $item->service->cardTemplate;
        $number = substr($phone, 2);
        $description = $item->notice;
        $service = $item->service->name;
        if ($description) {
            $service_and_description = $service . PHP_EOL . '---- ОПИСАНИЕ ----' . PHP_EOL . $description;
        } else {
            $service_and_description = $service;
        }
        // Templates field cases
        $values = [];
        $values[] = [
            'label' => 'ДАТА УСТАНОВКИ',
            'value' => date('Y-m-d'),
        ];
        $values[] = [
            'label' => 'ВЛАДЕЛЕЦ',
            'value' => $request->full_name,
        ];
        $values[] = [
            'label' => 'МАСТЕР',
            'value' => $request->master,
        ];
        $values[] = [
            'label' => 'МАРКА',
            'value' => $item->order->car,
        ];
        $values[] = [
            'label' => 'ГОС.НОМЕР',
            'value' => $request->car_number,
        ];
        $values[] = [
            'label' => 'ПРОДУКТ',
            'value' => $service_and_description,
        ];
        $values[] = [
            'label' => 'НОМЕР ПАРТИИ',
            'value' => $request->roll_number,
        ];

        $args['values'] = $values;

        $created_card_number = '';
        foreach ( range('A', 'Z') as $letter ) {
            $prefix = env('OSMICARD_NUMBER_PREFIX', 'G');
            $response = Osmicard::createSingleOsmicard($prefix.$letter.$number, $template->name, $args);

            if ( empty($response['RCODE']) && !empty($response['general']['serialNo']) && $response['general']['serialNo'] === $prefix.$letter.$number) {
                $created_card_number = $response['general']['serialNo'];
                break;
            }
        }

        if ( $created_card_number ) {
            $card = Card::create([
                'number' => $created_card_number,
            ]);
            $item->card()->save($card);

//            event(new CardCreated($card));

            return response()->json(['success' => true, 'card' => $created_card_number], 200);
        }

        return response()->json(['error' => 'К сожалению карта не была создана'], 200);
    }

    /**
     * Update warranty event card via AJAX.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateEventAjax(Request $request)
    {
        $rules = [
            'warranty_event' => 'required',
            'number' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ( $validator->fails() ) {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        $string = '<div>' . date('Y-m-d') . ': ' . $request->warranty_event . '</div>';
        $card = Osmicard::updateWarrantyEventSingleOsmicard($request->number, $string);

        if (!empty($card['general']['serialNo'])) {
            $new_event_val = Osmicard::getSingleOsmicardWarrantyEvent($card['general']['serialNo']);
            return response()->json(['success' => true, 'events' => $new_event_val], 200);
        } else {
            return response()->json([
                'success' => false,
                'errors' => 'Card was not updated'
            ]);
        }

    }

    /**
     * Get QR code via AJAX.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getQRCodeAjax(Request $request)
    {
        $link = Osmicard::getLinkOsmicard($request->number, 'qr');

        if (!empty($link['link'])) {
            return response()->json([
                'success' => true,
                'link' => $link['link']], 200);
        } else {
            return response()->json([
                'success' => false,
                'errors' => 'Card was not found'
            ], 404);
        }
    }

    /**
     * Send SMS via AJAX.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendSMSAjax(Request $request)
    {
        $card = Card::findOrFail($request->card_id);
        $user = $card->item->order->user;
        $link = Osmicard::getLinkOsmicard($card->number);
        $success = false;

        if (!empty($link['link'])) {
            $msg = 'GMASK. Гарантийная карта: ' . $link['link'];
            Notification::send($user, new CardCreatedNotification($msg, $card->number));
            $success = true;
        }

        return response()->json([
            'success' => $success,
        ], 200);
    }
}
