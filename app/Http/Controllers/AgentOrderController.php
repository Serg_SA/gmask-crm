<?php

namespace App\Http\Controllers;

use App\Agent;
use App\AgentOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AgentOrderController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'warranty_template' => 'required',
            'number' => 'required',
            'meters' => 'required',
            'agent_id' => 'required',
        ]);

        $order = new AgentOrder([
            'warranty_template' => $request->warranty_template,
            'number' => $request->number,
            'meters' => $request->meters,
            'meters_left' => $request->meters,
        ]);

        $agent = Agent::find($request->agent_id);
        $agent->orders()->save($order);

        return redirect()->back()->with([
            'message'    => __('voyager::generic.successfully_created'),
            'alert-type' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AgentOrder $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = AgentOrder::findOrFail($id);
        $deleted_success = $item->delete();

        if ($deleted_success) {
            return back()->with([
                'message'    => __('voyager::generic.successfully_deleted'),
                'alert-type' => 'success',
            ]);
        } else {
            return back()->with([
                'message'    => __('voyager::generic.error_deleting'),
                'alert-type' => 'error',
            ]);
        }
    }
}
