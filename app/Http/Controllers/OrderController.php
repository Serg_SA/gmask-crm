<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCarUserRequest;
use App\Http\Requests\UpdateCarUserRequest;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function list(Request $request)
    {
//        $search = $request->input('search', false);

//        $car_collection = Car::all();
//
//        if ($search) {
//            $filtered_cars = $car_collection->filter(function ($model) use ($search) {
//                return mb_stripos($model->model, $search) !== false;
//            });
//        } else {
//            $filtered_cars = $car_collection;
//        }
//
//        $results = [];
//        foreach ($filtered_cars as $car) {
//            $results[] = [
//                'id'   => $car->id,
//                'text' => $car->brand . ' ' . $car->model,
//            ];
//        }
//
//        return response()->json([
//            'results' => $results
//        ]);
    }

    public function storePhotos(Request $request)
    {
        $data = $request->validated();

        $user = User::find($data['user_id']);
        $car = Car::find($data['model']);

        $user->cars()->save($car, ['number' => $data['number']]);

        $car_user = DB::table('car_user')->where('number', $data['number'])->first();

        if ( $data['photo']) {
            $photos = explode(',', $data['photo']);
            foreach ($photos as $photo_name) {
                $public_path = 'cars/'.$photo_name;
                $data_photo = [
                    'car_user_id' => $car_user->id,
                    'path' => $public_path,
                    'filename' => $photo_name,
                    'size' => Storage::disk('public')->size($public_path),
                    'mime_type' => Storage::disk('public')->mimeType($public_path),
                ];
                Photo::create($data_photo);
            }
        }

        return redirect()->back()->with([
            'message'    => __('voyager::generic.successfully_updated').' '. __('profile.your_cars'),
            'alert-type' => 'success',
        ]);
    }

    public function updateStatus(Request $request)
    {
        $order_id = $request->order_id;
        $status = $request->status;
        $status_label = $request->status_label;

        $order = Order::findOrFail($order_id);
        if ( $order->id ) {
            $order->status = $status;
            $order->save();
            return response()->json([
                'data' => [
                    'status' => 200,
                    'status_label' => $status_label,
                    'message' => __('generic.status_successfully_updated'),
                ]
            ]);
        }

    }

    public function updatePhoto(UpdateCarUserRequest $request, $id)
    {
//        $data = $request->validated();
//
//        DB::table('car_user')->where('id', $id)->update(['number' => $data['number']]);
//
//        if ( $data['photo']) {
//            $photos = explode(',', $data['photo']);
//            foreach ($photos as $photo_name) {
//                $public_path = 'cars/'.$photo_name;
//                $data_photo = [
//                    'car_user_id' => $id,
//                    'path' => $public_path,
//                    'filename' => $photo_name,
//                    'size' => Storage::disk('public')->size($public_path),
//                    'mime_type' => Storage::disk('public')->mimeType($public_path),
//                ];
//                Photo::create($data_photo);
//            }
//        }
//
//        return redirect()->back()->with([
//            'message'    => __('voyager::generic.successfully_updated').' '. __('profile.your_cars'),
//            'alert-type' => 'success',
//        ]);
    }

    public function destroyPhoto($id)
    {
//        $car_user = CarUser::findOrFail($id);
//        $user = $car_user->user;
//        $photos = $car_user->photos;
//        if (count($photos) > 0) {
//            foreach ($photos as $photo) {
//                Storage::disk('local')->delete($photo->path);
//            }
//        }
//        $car_user->delete();
//
//        return redirect('admin/users/'.$user->id.'/edit')->with([
//            'message'    => __('voyager::generic.successfully_deleted').' '. __('profile.your_car'),
//            'alert-type' => 'success',
//        ]);
    }
}
