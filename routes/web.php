<?php

use App\Facades\Osmicard;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NexmoSMSController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->middleware('auth', 'verified');

Auth::routes(['verify' => true]);

Route::middleware('auth')->group(function () {
    Route::group(['prefix' => 'admin'], function () {

        Voyager::routes();

        Route::group(['middleware' => ['admin.user']], function () {

            Route::group(['prefix' => 'users', 'as' => 'admin.users.'], function () {
                Route::post('store_ajax',    ['as' => 'store_ajax',  'uses' => 'UserController@storeAjax']);
//            Route::delete('delete_car/{id}',    ['as' => 'delete_car',  'uses' => 'UserController@delete_car']);
                Route::post('select2',     ['as' => 'select2',        'uses' => 'UserController@select2']);
            });

            Route::group(['prefix' => 'agents', 'as' => 'admin.agents.'], function () {
                Route::post('approve_ajax',    ['as' => 'approve_ajax',  'uses' => 'AgentController@approveAjax']);
            });

            Route::group(['prefix' => 'dropzone', 'as' => 'admin.dropzone.'], function () {
                Route::get('/',        ['as' => 'index', 'uses' => 'PhotoController@index']);
                Route::post('store',   ['as' => 'store', 'uses' => 'PhotoController@store']);
            });

            Route::group(['prefix' => 'photos', 'as' => 'admin.photos.'], function () {
                Route::get('delete/{id}',        ['as' => 'delete', 'uses' => 'PhotoController@destroy']);
            });

            Route::group(['prefix' => 'orders', 'as' => 'admin.orders.'], function () {
                Route::post('photos',     ['as' => 'store_photos',  'uses' => 'OrderController@storePhotos']);
                Route::post('status',     ['as' => 'status',        'uses' => 'OrderController@updateStatus']);
            });

            Route::group(['prefix' => 'items', 'as' => 'admin.items.'], function () {
                Route::post('store',            ['as' => 'store',   'uses' => 'ItemController@store']);
                Route::delete('delete/{id}',    ['as' => 'delete',  'uses' => 'ItemController@destroy']);
                Route::put('update_ajax',        ['as' => 'update_ajax',  'uses' => 'ItemController@updateAjax']);
//            Route::post('create_card',      ['as' => 'create_card',  'uses' => 'ItemController@createCard']);
            });

            Route::group(['prefix' => 'agent_orders', 'as' => 'admin.agent_orders.'], function () {
                Route::post('store',            ['as' => 'store',   'uses' => 'AgentOrderController@store']);
                Route::delete('delete/{id}',    ['as' => 'delete',  'uses' => 'AgentOrderController@destroy']);
            });

            Route::group(['prefix' => 'card_templates', 'as' => 'admin.card_templates.'], function () {
                Route::get('fields',       ['as' => 'fields',   'uses' => 'CardTemplateController@fields']);
            });

            Route::group(['prefix' => 'cards', 'as' => 'admin.cards.'], function () {
                Route::post('store_ajax',       ['as' => 'store_ajax',   'uses' => 'CardController@storeAjax']);
                Route::post('update_event_ajax', ['as' => 'update_event_ajax',   'uses' => 'CardController@updateEventAjax']);
                Route::post('qrcode_ajax', ['as' => 'qrcode_ajax',   'uses' => 'CardController@getQRCodeAjax']);
                Route::post('send_sms_ajax', ['as' => 'send_sms_ajax',   'uses' => 'CardController@sendSMSAjax']);
            });

            Route::group(['prefix' => 'fields', 'as' => 'admin.fields.'], function () {
                Route::post('store_ajax',       ['as' => 'store_ajax',   'uses' => 'FieldController@storeAjax']);
                Route::delete('delete/{id}',    ['as' => 'delete',  'uses' => 'FieldController@destroy']);
            });

            Route::group(['prefix' => 'services', 'as' => 'admin.services.'], function () {
                Route::get('services/list', ['as' => 'list', 'uses' => 'ServiceController@list']);
            });

            Route::group(['prefix' => 'verify', 'as' => 'admin.verify.'], function () {
                Route::post('send/{phone}', ['as' => 'send', 'uses' => 'VerifyController@send']);
                Route::post('check/{code}', ['as' => 'check', 'uses' => 'VerifyController@check']);
            });

            Route::get('sendSMS', [NexmoSMSController::class, 'index']);

            Route::group(['prefix' => 'report', 'as' => 'admin.report.'], function () {
                Route::get('/',    ['as' => 'index',  'uses' => 'ReportController@index']);
                Route::get('/search',    ['as' => 'search',  'uses' => 'ReportController@search']);
            });
        });

    });
});


// Agents
Route::middleware(['auth'])->group(function () {
    Route::group(['prefix' => 'agent', 'as' => 'agent.'], function () {
        Route::group(['prefix' => 'cards', 'as' => 'cards.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'AgentCardController@index']);
            Route::get('show', ['as' => 'show', 'uses' => 'AgentCardController@show']);
            Route::post('store_ajax', ['as' => 'store_ajax',   'uses' => 'AgentCardController@storeAjax']);
            Route::post('update_event_ajax', ['as' => 'update_event_ajax',   'uses' => 'AgentCardController@updateEventAjax']);
            Route::post('get_card_ajax', ['as' => 'get_card_ajax',   'uses' => 'AgentCardController@getCardAjax']);
            Route::post('send_sms_ajax', ['as' => 'send_sms_ajax',   'uses' => 'AgentCardController@sendSMSAjax']);
            Route::post('qrcode_ajax', ['as' => 'qrcode_ajax',   'uses' => 'AgentCardController@getQRCodeAjax']);
        });
        Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {
            Route::post('store', ['as' => 'store',   'uses' => 'AgentOrderController@store']);
        });
        Route::get('dashboard', ['as' => 'dashboard',  'uses' => 'AgentDashboardController@index']);
    });
});

// Voyager
//Route::group(['as' => 'voyager.'], function () {
//    event(new TCG\Voyager\Events\Routing());
//
//    Route::get('login', ['uses' => 'Voyager\VoyagerAuthController@login', 'as' => 'login']);
//
//    event(new TCG\Voyager\Events\RoutingAfter());
//});

// Some test
//Route::get('phpinfo', function () {
//    phpinfo();
//});
