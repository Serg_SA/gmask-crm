<?php

use App\User;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('import_users', function () {
    (new DatabaseSeeder())->call(UsersTableSeeder::class);
})->describe('Import users from Seeder');

Artisan::command('test', function () {
//    print_r(env('APP_NAME'));
//    print_r(env('MAIL_FROM_ADDRESS'));
//    print_r(1);
})->describe('Display an inspiring quote');